#ifndef PnnNA62Event_h
#define PnnNA62Event_h

#include "TObject.h"
#include "TClass.h"
#include "TClonesArray.h"
#include "TString.h"
#include "TVector3.h"

///////////////////////
// NA62 Trigger ///////
///////////////////////
class PnnNA62Trigger : public TObject {
public:
    PnnNA62Trigger();
    virtual ~PnnNA62Trigger();
    void Clear(Option_t* option="");

public:
    bool GetNewCHODL0() const {return fNewCHODL0;};
    bool GetQxL0() const {return fQxL0;};
    bool GetUTMCL0() const {return fUTMCL0;};
    bool GetMUV3L0() const {return fMUV3L0;};
    bool GetLAVL0() const {return fLAVL0;};
    bool GetLKrL0() const {return fLKrL0;};
    bool GetRICHL0() const {return fRICHL0;};
    bool GetRICHRef() const {return fRICHRef;};
    bool GetNewCHODRef() const {return fNewCHODRef;};
    bool GetIsTrigger() const {return fIsTrigger;};
    void SetNewCHODL0(bool val) {fNewCHODL0=val;};
    void SetQxL0(bool val) {fQxL0=val;};
    void SetUTMCL0(bool val) {fUTMCL0=val;};
    void SetMUV3L0(bool val) {fMUV3L0=val;};
    void SetLAVL0(bool val) {fLAVL0=val;};
    void SetLKrL0(bool val) {fLKrL0=val;};
    void SetRICHL0(bool val) {fRICHL0=val;};
    void SetRICHRef(bool val) {fRICHRef=val;};
    void SetNewCHODRef(bool val) {fNewCHODRef=val;};
    void SetIsTrigger(bool val) {fIsTrigger=val;};

private:
    bool fNewCHODL0;
    bool fQxL0;
    bool fUTMCL0;
    bool fMUV3L0;
    bool fLAVL0;
    bool fLKrL0;
    bool fRICHL0;
    bool fRICHRef;
    bool fNewCHODRef;
    bool fIsTrigger;

    ClassDef(PnnNA62Trigger,2);
};

////////////////////////////////
//  NA62 Upstream Track /////////
////////////////////////////////
class PnnNA62UpstreamParticle : public TObject {
public:
    PnnNA62UpstreamParticle();
    virtual ~PnnNA62UpstreamParticle();
    void Clear(Option_t *option="");

//New addition!
public:
    short GetGTKExtraHits() const {return fGTKExtraHits;}
    void SetGTKExtraHits(short val) {fGTKExtraHits = val;}
private:
    short fGTKExtraHits;
public:
    TVector3 GetP()           const { return fP; };
    double GetTKTAG()         const { return fTKTAG; };
    double GetTGTK()         const { return fTGTK; };
    double GetTGTK1()         const { return fTGTK1; };
    double GetTGTK2()         const { return fTGTK2; };
    double GetTGTK3()         const { return fTGTK3; };
    double GetTCHANTI()       const { return fTCHANTI; };
    float GetPosGTK1X()     const { return fPosGTK1X; };
    float GetPosGTK1Y()     const { return fPosGTK1Y; };
    float GetPosGTK2X()     const { return fPosGTK2X; };
    float GetPosGTK2Y()     const { return fPosGTK2Y; };
    float GetPosGTK3X()     const { return fPosGTK3X; };
    float GetPosGTK3Y()     const { return fPosGTK3Y; };

    int GetNGTKTracks()       const { return fNGTKTracks; };
    short GetNGTKInTimeTracks()       const { return fNGTKInTimeTracks; };
    void SetP(TVector3 val) { fP=val; };
    void SetTKTAG(double val) { fTKTAG=val; };
    void SetTGTK(double val) { fTGTK=val; };
    void SetTGTK1(double val) { fTGTK1=val; };
    void SetTGTK2(double val) { fTGTK2=val; };
    void SetTGTK3(double val) { fTGTK3=val; };
    void SetTCHANTI(double val) { fTCHANTI=val; };
    void SetPosGTK1X(float val) { fPosGTK1X=val; };
    void SetPosGTK1Y(float val) { fPosGTK1Y=val; };
    void SetPosGTK2X(float val) { fPosGTK2X=val; };
    void SetPosGTK2Y(float val) { fPosGTK2Y=val; };
    void SetPosGTK3X(float val) { fPosGTK3X=val; };
    void SetPosGTK3Y(float val) { fPosGTK3Y=val; };

    void SetNGTKTracks(int val) { fNGTKTracks=val; };
    void SetNGTKInTimeTracks(short val) { fNGTKInTimeTracks=val; };

private:
    TVector3 fP;
    double fTKTAG;
    double fTGTK;
    double fTGTK1;
    double fTGTK2;
    double fTGTK3;
    double fTCHANTI;
    float fPosGTK1X;
    float fPosGTK1Y;
    float fPosGTK2X;
    float fPosGTK2Y;
    float fPosGTK3X;
    float fPosGTK3Y;
    int fNGTKTracks;
    short fNGTKInTimeTracks;

    ClassDef(PnnNA62UpstreamParticle,2);
};

////////////////////////////////
// NA62 Downstream Track ///////
////////////////////////////////
class PnnNA62DownstreamParticle : public TObject {
public:
    PnnNA62DownstreamParticle();
    virtual ~PnnNA62DownstreamParticle();
    void Clear(Option_t *option="");

public:
    TVector3 GetP()                 const { return fP; };
    double   GetSlopeXAfterMagnet() const { return fSlopeXAfterMagnet; };
    double   GetSlopeYAfterMagnet() const { return fSlopeYAfterMagnet; };
    double   GetPositionXAfterMagnet() const { return fPositionXAfterMagnet; };
    double   GetPositionYAfterMagnet() const { return fPositionYAfterMagnet; };
    double   GetPositionZAfterMagnet() const { return fPositionZAfterMagnet; };
    double   GetTSpectrometer()     const { return fTSpectrometer; };
    double   GetTRICH()             const { return fTRICH; };
    double   GetTrackCentredTRICH() const { return fTrackCentredTRICH; };
    double   GetTCHOD()             const { return fTCHOD; };
    double   GetTNewCHOD()          const { return fTNewCHOD; };
    double   GetTLKr()              const { return fTLKr; };

    short    GetIsMuon()            const { return fIsMuon; };
    bool     GetIsMip()             const { return fIsMip; };
    bool     GetIsMulti()           const { return fIsMulti; };
    bool     GetIsPositron()        const { return fIsPositron; };
    bool     GetIsMUV3()            const { return fIsMUV3; };
    double   GetCaloMuonProb()      const { return fCaloMuonProb; };
    double   GetCaloPionProb()      const { return fCaloPionProb; };
    double   GetCaloEnergy()        const { return fCaloEnergy; };
    double   GetMUV1Energy()        const { return fMUV1Energy; };
    double   GetMUV2Energy()        const { return fMUV2Energy; };
    bool     GetIsGoodMUV1()        const {return fIsGoodMUV1;};
    bool     GetIsGoodMUV2()        const {return fIsGoodMUV2;};

    double   GetLKrEnergy()         const { return fLKrEnergy; };
    double   GetLKrSeedEnergy()     const { return fLKrSeedEnergy; };
    int      GetLKrNCells()         const { return fLKrNCells; };

    float    GetDownstreamDistance() const{ return fDownstreamDistance;}

    bool     GetIsRICHLike()        const { return fIsRICHLike; };
    bool     GetIsRICHSingleRing()  const { return fIsRICHSingleRing; };
    short    GetRICHSingleNHits()   const { return fRICHSingleNHits; };
    short    GetRICHLikeMost()      const { return fRICHLikeMost; };
    double   GetRICHLikeProb(int i) const { return fRICHLikeProb[i]; };
    double   GetRICHRadius()        const { return fRICHRadius; };
    double   GetTrackCentredRICHRadius() const { return fTrackCentredRICHRadius; };
    double   GetTrackCentredRICHMass()   const { return fTrackCentredRICHMass; };
    double   GetRICHMass()          const { return fRICHMass; };
    double   GetRICHMomentum()      const { return fRICHMomentum; };
    double   GetPosTrim5X()          const { return fPosTrim5X; };
    double   GetPosTrim5Y()          const { return fPosTrim5Y; };
    double   GetPosGTK3X()           const { return fPosGTK3X; };
    double   GetPosGTK3Y()           const { return fPosGTK3Y; };
    double   GetPosStraw1X()         const { return fPosStraw1X; };
    double   GetPosStraw1Y()         const { return fPosStraw1Y; };

    void SetP(TVector3 val) { fP=val; };
    void SetSlopeXAfterMagnet(double val) { fSlopeXAfterMagnet=val; };
    void SetSlopeYAfterMagnet(double val) { fSlopeYAfterMagnet=val; };
    void SetPositionXAfterMagnet(double val) { fPositionXAfterMagnet=val; };
    void SetPositionYAfterMagnet(double val) { fPositionYAfterMagnet=val; };
    void SetPositionZAfterMagnet(double val) { fPositionZAfterMagnet=val; };
    void SetTSpectrometer(double val) { fTSpectrometer=val; };
    void SetTRICH(double val) { fTRICH=val; };
    void SetTrackCentredTRICH(double val) { fTrackCentredTRICH=val; };
    void SetTCHOD(double val) { fTCHOD=val; };
    void SetTNewCHOD(double val) { fTNewCHOD=val; };
    void SetTLKr(double val) { fTLKr=val; };
    void SetIsMuon(short val) { fIsMuon=val; };
    void SetIsMip(bool val) { fIsMip=val; };
    void SetIsMulti(bool val) { fIsMulti=val; };
    void SetIsPositron(bool val) { fIsPositron=val; };
    void SetIsMUV3(bool val) { fIsMUV3=val; };
    void SetCaloMuonProb(double val) { fCaloMuonProb=val; };
    void SetCaloPionProb(double val) { fCaloPionProb=val; };
    void SetCaloEnergy(double val) { fCaloEnergy=val; };
    void SetMUV1Energy(double val) { fMUV1Energy=val; };
    void SetMUV2Energy(double val) { fMUV2Energy=val; };
    void SetIsGoodMUV1(bool val) {fIsGoodMUV1=val;};
    void SetIsGoodMUV2(bool val) {fIsGoodMUV2=val;};
    void SetLKrEnergy(double val) { fLKrEnergy=val; };
    void SetLKrSeedEnergy(double val) { fLKrSeedEnergy=val; };
    void SetLKrNCells(int val) { fLKrNCells=val; };
    void SetDownstreamDistance(float val){fDownstreamDistance = val;}
    void SetIsRICHLike(bool val) { fIsRICHLike=val; };
    void SetIsRICHSingleRing(bool val) { fIsRICHSingleRing=val; };
    void SetRICHSingleNHits(short val) { fRICHSingleNHits=val; };
    void SetRICHLikeMost(short val) { fRICHLikeMost=val; };
    void SetRICHLikeProb(int i, double val) { fRICHLikeProb[i]=val; };
    void SetRICHRadius(double val) { fRICHRadius=val; };
    void SetTrackCentredRICHRadius(double val) { fTrackCentredRICHRadius=val; };
    void SetRICHMass(double val) { fRICHMass=val; };
    void SetTrackCentredRICHMass(double val) { fTrackCentredRICHMass=val; };
    void SetRICHMomentum(double val) { fRICHMomentum=val; };
    void SetPosTrim5X(double val) { fPosTrim5X=val; };
    void SetPosTrim5Y(double val) { fPosTrim5Y=val; };
    void SetPosGTK3X(double val) { fPosGTK3X=val; };
    void SetPosGTK3Y(double val) { fPosGTK3Y=val; };
    void SetPosStraw1X(double val) { fPosStraw1X=val; };
    void SetPosStraw1Y(double val) { fPosStraw1Y=val; };

private:
    TVector3 fP;
    double fSlopeXAfterMagnet;
    double fSlopeYAfterMagnet;
    double fPositionXAfterMagnet;
    double fPositionYAfterMagnet;
    double fPositionZAfterMagnet;
    double fTSpectrometer;
    double fTRICH;
    double fTCHOD;
    double fTNewCHOD;
    double fTLKr;
    short fIsMuon;
    bool fIsMip;
    bool fIsMulti;
    bool fIsPositron;
    bool fIsMUV3;
    double fCaloMuonProb;
    double fCaloPionProb;
    double fCaloEnergy;
    double fMUV1Energy;
    double fMUV2Energy;
    bool fIsGoodMUV1;
    bool fIsGoodMUV2;
    double fLKrEnergy;
    double fLKrSeedEnergy;
    int fLKrNCells;
    float fDownstreamDistance;
    bool fIsRICHLike;
    bool fIsRICHSingleRing;
    short fRICHSingleNHits;
    short fRICHLikeMost;
    double fRICHLikeProb[5];
    double fRICHRadius;
    double fTrackCentredRICHMass;
    double fTrackCentredRICHRadius;
    double fTrackCentredTRICH;
    double fRICHMass;
    double fRICHMomentum;
    double fPosTrim5X;
    double fPosTrim5Y;
    double fPosGTK3X;
    double fPosGTK3Y;
    double fPosStraw1X;
    double fPosStraw1Y;

    ClassDef(PnnNA62DownstreamParticle,2);
};


///////////////////////////////////
// Class NA62Event ////////////////
///////////////////////////////////
class PnnNA62Event : public TObject {
public:
    PnnNA62Event();
    virtual ~PnnNA62Event();
    void Clear(Option_t *option="");

public:
    short GetBurstNumber()    const {return fBurstNumber;   };
    short GetRunNumber()      const {return fRunNumber;     };
    int GetEventNumber() const {return fEventNumber;};
    double GetTimestamp()   const {return fTimestamp;     };
    short GetTriggerMask()    const {return fTriggerMask;   };
    void SetBurstNumber(short val) {fBurstNumber=val;};
    void SetRunNumber(short val) {fRunNumber=val;};
    void SetEventNumber(int val) {fEventNumber=val;};
    void SetTimestamp(double val) {fTimestamp=val;};
    void SetTriggerMask(short val) {fTriggerMask=val;};

    bool GetIsK2piEvent()              const {return fIsK2piEvent;             };
    float GetLambda()                  const {return fLambda;                  };
    float GetLambdaFW()                const {return fLambdaFW;                };
    float GetCDA()                     const {return fCDA;                     };
    TVector3 GetVertex()               const {return fVertex;                  };
    double GetMMiss()                  const {return fMMiss;                   };
    double GetMMissRich()              const {return fMMissRich;               };
    double GetMMissNominalK()          const {return fMMissNominalK;           };
    double GetMmuMiss()                const {return fMmuMiss;                 };
    short GetIsHighToT()                const {return fIsHighToT;               };
    bool GetIsInteraction()            const {return fIsInteraction;           };
    bool GetIsEarlyDecay()             const {return fIsEarlyDecay;            };
    bool GetIsSignalInCHANTI()         const {return fIsSignalInCHANTI;        };
    double GetIsKTAGBeamDiscriminant() const {return fIsKTAGBeamDiscriminant;  };
    double GetIsRICHBeamDiscriminant() const {return fIsRICHBeamDiscriminant;  };
    bool GetIsLKrInTimeHits()          const {return fIsLKrInTimeHits;         };
    double GetLKrInTimeEnergy()        const {return fLKrInTimeEnergy;         };
    void SetIsK2piEvent(bool val)              {fIsK2piEvent=val;             };
    void SetLambda(float val)                 {fLambda=val;                  };
    void SetLambdaFW(float val)               {fLambdaFW=val;                };
    void SetVertex(TVector3 val)               {fVertex=val;                  };
    void SetCDA(float val)                     {fCDA=val;                     };
    void SetMMiss(double val)                  {fMMiss=val;                   };
    void SetMMissRich(double val)              {fMMissRich=val;               };
    void SetMMissNominalK(double val)          {fMMissNominalK=val;           };
    void SetMmuMiss(double val)                {fMmuMiss=val;                 };
    void SetIsHighToT(short val)                {fIsHighToT=val;               };
    void SetIsInteraction(bool val)            {fIsInteraction=val;           };
    void SetIsEarlyDecay (bool val)            {fIsEarlyDecay=val;            };
    void SetIsSignalInCHANTI (bool val)        {fIsSignalInCHANTI=val;        };
    void SetIsKTAGBeamDiscriminant(double val) {fIsKTAGBeamDiscriminant=val;  };
    void SetIsRICHBeamDiscriminant(double val) {fIsRICHBeamDiscriminant=val;  };
    void SetIsLKrInTimeHits(bool val)          {fIsLKrInTimeHits=val;           };
    void SetLKrInTimeEnergy(double val)        {fLKrInTimeEnergy=val;           };

    void SetSACToT_TH_LH(bool val)   { fSACToT_TH_LH = val;};
    void SetIRCToT_TH_LH(bool val)   { fIRCToT_TH_LH = val;};
    void SetSACToT_TL_TH(bool val)   { fSACToT_TL_TH = val;};
    void SetIRCToT_TL_TH(bool val)   { fIRCToT_TL_TH = val;};

    void SetSACToT(double val)       { fSACToT = val;};
    void SetIRCToT(double val)       { fIRCToT = val;};
    void SetSACEnergyADC(double val) { fSACEnergyADC = val;};
    void SetIRCEnergyADC(double val) { fIRCEnergyADC = val;};

    void SetSACTimeTDC(double val)   { fSACTimeTDC = val;};
    void SetIRCTimeTDC(double val)   { fIRCTimeTDC = val;};

    void SetSACTimeADC(double val)   { fSACTimeADC = val;};
    void SetIRCTimeADC(double val)   { fIRCTimeADC = val;};

    bool GetSACToT_TH_LH()  const {return fSACToT_TH_LH;};
    bool GetIRCToT_TH_LH()  const {return fIRCToT_TH_LH;};
    bool GetSACToT_TL_TH()  const {return fSACToT_TL_TH;};
    bool GetIRCToT_TL_TH()  const {return fIRCToT_TL_TH;};

    double GetSACToT()      const {return fSACToT;};
    double GetIRCToT()      const {return fIRCToT;};
    double GetSACEnergyADC()const {return fSACEnergyADC;};
    double GetIRCEnergyADC()const {return fIRCEnergyADC;};

    double GetSACTimeTDC()  const {return fSACTimeTDC;};
    double GetIRCTimeTDC()  const {return fIRCTimeTDC;};
    double GetSACTimeADC()  const {return fSACTimeADC;};
    double GetIRCTimeADC()  const {return fIRCTimeADC;};

    bool GetIsPhotonInLKr()  const {return fIsPhotonInLKr;};
    bool GetIsSignalInLAV()  const {return fIsSignalInLAV;};
    bool GetIs1PhotonInLAV() const {return fIs1PhotonInLAV;};
    bool GetIs2PhotonsInLAV()const {return fIs2PhotonsInLAV;};
    bool GetIsSignalInIRC()  const {return fIsSignalInIRC;};
    bool GetIsSignalInSAC()  const {return fIsSignalInSAC;};
    bool GetIsSignalInSAV()  const {return fIsSignalInSAV;};
    bool GetIsAuxiliaryLKr() const {return fIsAuxiliaryLKr;};
    void SetIsPhotonInLKr(bool val) {fIsPhotonInLKr=val;};
    void SetIsSignalInLAV(bool val)  {fIsSignalInLAV=val;};
    void SetIs1PhotonInLAV(bool val)  {fIs1PhotonInLAV=val;};
    void SetIs2PhotonsInLAV(bool val)  {fIs2PhotonsInLAV=val;};
    void SetIsSignalInIRC(bool val) {fIsSignalInIRC=val;};
    void SetIsSignalInSAC(bool val) {fIsSignalInSAC=val;};
    void SetIsSignalInSAV(bool val) {fIsSignalInSAV=val;};
    void SetIsAuxiliaryLKr(bool val) {fIsAuxiliaryLKr=val;};

    bool GetIsSegment()          const {return fIsSegment;};
    bool GetIsOneParticle()      const {return fIsOneParticle;};
    bool GetIsBroadMultiVertex() const {return fIsBroadMultiVertex;};
    bool GetIsMultiplicity()     const {return fIsMultiplicity;};
    UInt_t GetPhotonVetoTimeFlag() const{return fPhotonVetoTimeFlag;}
    short  GetMultAllOldCH7()      const {return fMultAllOldCH7;};
    short  GetMultOldCHLKr()       const {return fMultOldCHLKr;};
    short  GetMultOldNewCH()       const {return fMultOldNewCH;};
    short  GetMultNewCHLKr()       const {return fMultNewCHLKr;};
    short  GetMultMUV0()           const {return fMultMUV0;};
    short  GetMultHAC()            const {return fMultHAC;};
    bool GetMultLKrMerged()      const {return fMultLKrMerged;};
    void SetIsSegment (bool val) {fIsSegment=val;};
    void SetIsMultiplicity (bool val) {fIsMultiplicity=val;};
    void SetPhotonVetoTimeFlag(UInt_t val) {fPhotonVetoTimeFlag=val;}
    void SetIsOneParticle (bool val) {fIsOneParticle=val;};
    void SetIsBroadMultiVertex (bool val) {fIsBroadMultiVertex=val;};
    void SetMultAllOldCH7(short val) {fMultAllOldCH7=val;};
    void SetMultOldCHLKr(short val) {fMultOldCHLKr=val;};
    void SetMultLKrMerged(bool val) {fMultLKrMerged=val;};
    void SetMultOldNewCH(short val) {fMultOldNewCH=val;};
    void SetMultNewCHLKr(short val) {fMultNewCHLKr=val;};
    void SetMultMUV0(short val) {fMultMUV0=val;};
    void SetMultHAC(short val) {fMultHAC=val;};
    PnnNA62DownstreamParticle *GetDownstreamParticle() {return fDownstreamParticle;};
    void SetDownstreamParticle(PnnNA62DownstreamParticle *val) {fDownstreamParticle=val;};

    PnnNA62UpstreamParticle *GetUpstreamParticle() {return fUpstreamParticle;};
    void SetUpstreamParticle(PnnNA62UpstreamParticle *val) {fUpstreamParticle=val;};

    PnnNA62Trigger *GetTrigger() {return fTrigger;};
    void SetTrigger(PnnNA62Trigger *val) {fTrigger=val;};

private:
    short fBurstNumber;
    short fRunNumber;
    int fEventNumber;
    double fTimestamp;
    short fTriggerMask;

    bool fIsK2piEvent;
    float fLambda;
    float fLambdaFW;
    TVector3 fVertex;
    float fCDA;
    double fMMiss;
    double fMMissRich;
    double fMMissNominalK;
    double fMmuMiss;
    short fIsHighToT;
    bool fIsInteraction;
    bool fIsEarlyDecay;
    bool fIsSignalInCHANTI;
    double fIsKTAGBeamDiscriminant;
    double fIsRICHBeamDiscriminant;
    bool fIsLKrInTimeHits;
    double fLKrInTimeEnergy;
    bool fIsPhotonInLKr;
    bool fIs2PhotonsInLAV;
    bool fIs1PhotonInLAV;
    bool fIsSignalInLAV;
    bool fIsSignalInIRC;
    bool fIsSignalInSAC;
    bool fIsSignalInSAV;
    bool fIsAuxiliaryLKr;

    double fSACToT;
    bool   fSACToT_TH_LH;
    bool   fSACToT_TL_TH;
    double fIRCToT;
    bool   fIRCToT_TH_LH;
    bool   fIRCToT_TL_TH;
    double fSACEnergyADC;
    double fIRCEnergyADC;
    double fSACTimeTDC;
    double fIRCTimeTDC;
    double fSACTimeADC;
    double fIRCTimeADC;

    bool fIsSegment;
    bool fIsMultiplicity;
    bool fIsOneParticle;
    bool fIsBroadMultiVertex;
    short fMultAllOldCH7;
    short fMultOldCHLKr;
    short fMultOldNewCH;
    short fMultNewCHLKr;
    short fMultMUV0;
    short fMultHAC;
    bool fMultLKrMerged;

    UInt_t fPhotonVetoTimeFlag;

    PnnNA62DownstreamParticle *fDownstreamParticle;
    PnnNA62UpstreamParticle *fUpstreamParticle;
    PnnNA62Trigger *fTrigger;

    ClassDef(PnnNA62Event,1);
};

#endif
