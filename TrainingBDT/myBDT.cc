#include <cstdlib>
#include <stdlib.h>
#include <iostream>
#include <map>
#include <string>
#include "TChain.h"
#include "TFile.h"
#include "TTree.h"
#include "TString.h"
#include "TObjString.h"
#include "TSystem.h"
#include "TROOT.h"
#include "TF1.h"
#include "TCanvas.h"
#include "TMVA/Factory.h"
#include "TMVA/Tools.h"
#include "TMVA/TMVAGui.h"
#include "TMVA/DataLoader.h"
#include "TMVA/MethodBase.h"
#include "TGraphErrors.h"
// #include "TMVA/IMethod.h"

void myBDT(){

  TString fnameSig = "./SignalForBDT.root";
  TFile *inputSig = TFile::Open( fnameSig );
  TString fnameBkg = "./BkgForBDT.root";
  TFile *inputBkg = TFile::Open( fnameBkg );
  TTree *signal = (TTree*)inputSig->Get("PNNHisto/fTreeSig");
  TTree *background = (TTree*)inputBkg->Get("PNNHisto/fTreeBkg");

  TString outfileName( "myBDToutput.root" );
  TFile* outputFile = TFile::Open(outfileName, "RECREATE");

  TMVA::Tools::Instance();

  TMVA::Factory *factory = new TMVA::Factory( "TMVAClassification", outputFile,"!V:!Silent:Color:DrawProgressBar:Transformations=I;D;P;G,D:AnalysisType=Classification" );

  TMVA::DataLoader *dataloader = new TMVA::DataLoader();

  //  dataloader->AddVariable("Ptrack",'F');
  //  dataloader->AddVariable("M2Miss",'F'); 
  dataloader->AddVariable("Xtrim5",'F');
  dataloader->AddVariable("Ytrim5",'F');
  dataloader->AddVariable("Xstraw1",'F');
  dataloader->AddVariable("Ystraw1",'F');
  dataloader->AddVariable("Rstraw1",'F');
  dataloader->AddVariable("Xvtx",'F');
  dataloader->AddVariable("Yvtx",'F');
  dataloader->AddVariable("Zvtx",'F');
  dataloader->AddVariable("ThetaX",'F');
  dataloader->AddVariable("ThetaY",'F');
  //  dataloader->AddVariable("Lambda",'F');
  //  dataloader->AddVariable("cda",'F');

  Double_t signalWeight     = 1.0;
  Double_t backgroundWeight = 1.0;

  dataloader->AddSignalTree    ( signal,     signalWeight     );
  dataloader->AddBackgroundTree( background, backgroundWeight );

  // dataloader->SetSignalWeightExpression( "weight" );
  // dataloader->SetBackgroundWeightExpression( "weight" );

  // Apply additional cuts on the signal and background samples (can be different)
  TCut mycuts = ""; // for example: TCut mycuts = "abs(var1)<0.5 && abs(var2-0.5)<1";
  TCut mycutb = ""; // for example: TCut mycutb = "abs(var1)<0.5";

  dataloader->PrepareTrainingAndTestTree( mycuts, mycutb, "nTrain_Signal=0:nTrain_Background=0:nTest_Signal=0:nTest_Background=0:SplitMode=Random:NormMode=NumEvents:!V" );
  
  factory->BookMethod(dataloader, TMVA::Types::kBDT, "BDT","!H:!V:NTrees=100:MinNodeSize=2.5%:MaxDepth=5:BoostType=AdaBoost:AdaBoostBeta=0.2:nCuts=20:NegWeightTreatment=IgnoreNegWeightsInTraining" );

  factory->TrainAllMethods();
  factory->TestAllMethods();
  factory->EvaluateAllMethods();

  outputFile->Close();

  TH1D* h;
  TFile* file = new TFile("myBDToutput.root");
  
  for(Int_t j=100; j<=100; j++){ // from ~ 1% to 0.1% 
    //    Double_t BkgEff = 0.011 - j*0.0001;
    Double_t BkgEff = 0.001;
    cout<< " ------------------------------------------------------------------------------- "<<endl;
    cout<<"BkgEff = "<<BkgEff<<endl;
    Double_t errEff001 = 999999999.9;
    Double_t Eff001;
    Eff001 = (dynamic_cast<TMVA::MethodBase*>(factory->GetMethod("default","BDT")))->GetEfficiency(Form("Efficiency:%g",BkgEff),TMVA::Types::kTesting,errEff001);
    cout<<"Eff001 (testing) = "<<Eff001<<" +- "<<errEff001<<endl;
    errEff001 = 999999999.9;
    Eff001 = (dynamic_cast<TMVA::MethodBase*>(factory->GetMethod("default","BDT")))->GetTrainingEfficiency(Form("Efficiency:%g",BkgEff));
    cout<<"Eff001 (training) = "<<Eff001<<" +- "<<errEff001<<endl;

    cout<<"------ MANUAL COMPUTATION ------"<<endl;
    h = (TH1D*)file->Get("default/Method_BDT/BDT/MVA_BDT_B_high");
    Double_t Nentries = 1.0*h->Integral();
    cout<<"Nentries = "<<Nentries<<endl;
    Int_t Nbins = h->GetNbinsX();
    cout<<"Nbins = "<<Nbins<<endl;
    Double_t Efficiency=0.0;
    Bool_t stop = false;
    Int_t iBin = 1;
    Double_t BestCut = 0.0;
    while(!stop && (iBin<Nbins)){
      Efficiency = 1.0 - h->Integral(1,iBin)/Nentries;
      if((Efficiency)>BkgEff){
        iBin++;
      }
      else{
        stop = true;
        BestCut = h->GetBinCenter(iBin)+0.5*h->GetBinWidth(iBin);
      }
    }
    Nentries = 1.0*h->GetEntries();
    Double_t errEff = sqrt(Efficiency*(1.0-Efficiency)/Nentries);
    cout<<"iBin = "<<iBin<<endl;
    cout<<"Efficiency_Background = "<<Efficiency<<" +- "<<errEff<<endl;
    cout<<"BestCut = "<<BestCut<<endl;

    h = (TH1D*)file->Get("default/Method_BDT/BDT/MVA_BDT_S_high");
    Nentries = 1.0*h->Integral();
    Efficiency = 1.0 - h->Integral(1,iBin)/Nentries;
    Nentries = 1.0*h->GetEntries();
    errEff = sqrt(Efficiency*(1.0-Efficiency)/Nentries);
    cout<<"Efficiency_Signal = "<<Efficiency<<" +- "<<errEff<<endl;

  }

}

void ComputeEff(){
  TH1D* h; TH1D* h1;
  TFile* file = new TFile("myBDToutput.root");
  h = (TH1D*)file->Get("default/Method_BDT/BDT/MVA_BDT_B_high");
  TFile* file1 = new TFile("Run_2018_DEFGH.root");
  h1 = (TH1D*)file1->Get("PNNHisto/BDT_bkg_Classifier");

  TGraphErrors* graph = new TGraphErrors();
  graph->SetName("graph");
  graph->Set(100);
  graph->GetYaxis()->SetTitle("Bkg efficiency");
  graph->GetXaxis()->SetTitle("Classifier cut");
  graph->GetXaxis()->SetRangeUser(-0.5,0.9);
  graph->GetYaxis()->SetRangeUser(0.0,0.2);
  graph->SetLineColor(kRed);

  TGraphErrors* graph1 = new TGraphErrors();
  graph1->SetName("graph1");
  graph1->Set(100);
  graph1->GetYaxis()->SetTitle("Bkg efficiency");
  graph1->GetXaxis()->SetTitle("Classifier cut");
  graph->GetXaxis()->SetRangeUser(-0.5,0.9);
  graph->GetYaxis()->SetRangeUser(0.0,0.2);
  graph->SetLineColor(kBlue);

  TGraphErrors* graphR = new TGraphErrors();
  graphR->SetName("graphR");
  graphR->Set(100);
  graphR->GetYaxis()->SetTitle("Bkg eff Ratio");
  graphR->GetXaxis()->SetTitle("Classifier cut");
  graphR->SetLineColor(kRed);

  Double_t Nentries;
  Int_t Nbins = h->GetNbinsX();
  Double_t Nentries1 = 1.0*h1->Integral();
  Int_t Nbins1 = h1->GetNbinsX();
  Double_t Eff=9999.9;
  Double_t errEff=9999.9;
  Double_t Eff1=9999.9;
  Double_t errEff1=9999.9;
  Double_t BinX = 9999.9;
  Double_t BinX_temp = 9999.9;
  Int_t myBin = 99999999;
  // for(Int_t iBin=110; iBin<Nbins1; iBin++){
  for(Int_t iBin=110; iBin<171; iBin++){ // up to 0.6
    // running sample
    Eff1 = 1.0 - h1->Integral(1,iBin)/Nentries1;
    errEff1 = sqrt(Eff1*(1.0-Eff1)/Nentries1);
    if(Eff1<0.0000001) break;
    BinX = -1.1 + 0.01*iBin;
    graph1->SetPoint(iBin, BinX,Eff1);
    graph1->SetPointError(iBin, 0.,errEff1);

    // training sample
    myBin =99999999;
    for(Int_t jBin=1000; jBin<Nbins; jBin++){
      BinX_temp = h->GetBinCenter(jBin)+0.5*h->GetBinWidth(jBin);
      if (BinX_temp >= BinX){
	myBin=jBin;
	break;
      }
    }
    Nentries = 1.0*h->Integral();
    Eff = 1.0 - h->Integral(1,myBin)/Nentries;
    if(Eff<0.0000001) break;
    Nentries = 1.0*h->GetEntries();
    errEff = sqrt(Eff*(1.0-Eff)/Nentries);
    graph->SetPoint(iBin, BinX,Eff);
    graph->SetPointError(iBin, 0.,errEff);

    Double_t Ratio = Eff1/Eff;
    Double_t errRatio = Ratio*sqrt( (errEff/Eff)*(errEff/Eff) + (errEff1/Eff1)*(errEff1/Eff1) );
    graphR->SetPoint(iBin, BinX,Ratio);
    graphR->SetPointError(iBin, 0.,errRatio);

  }

  TCanvas* C2 = new TCanvas("C2");
  graphR->GetYaxis()->SetRangeUser(1.0,2.4);
  graphR->Draw("AP");
  graphR->Fit("pol0","","",0.4,0.55);

  TCanvas* C1 = new TCanvas("C1");
  graph1->GetYaxis()->SetTitleOffset(1.4);
  graph->GetYaxis()->SetTitleOffset(1.4);
  graph1->Draw("AP");
  graph->Draw("P");
  TLegend *leg = new TLegend(0.5,0.5,0.9,0.9);
  leg->AddEntry(graph,"Out of time (training)","f");
  leg->AddEntry(graph1,"In time (new sample C)","f");
  leg->SetBorderSize(1);
  leg->Draw();

  return;
}
