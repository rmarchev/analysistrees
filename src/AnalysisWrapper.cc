#include "AnalysisWrapper.hh"

AnalysisWrapper::AnalysisWrapper() {}
AnalysisWrapper::~AnalysisWrapper() {}

void AnalysisWrapper::PMIN(PMINNNAnal *an) {
  cout << "############# Results from PIMINNN Analysis #############" << endl;
}
void AnalysisWrapper::K2PI(K2piAnal *an) {

  cout << "############# Results from PiP0 Analysis #############" << endl;

}

// void AnalysisWrapper::Kmu2(Kmu2Analysis *an) {
void AnalysisWrapper::KMU2(Kmu2Anal *an) {

  cout << "############# Results from Kmu2 Analysis #############" << endl;

  }




void AnalysisWrapper::NORMK2PI(K2piNormAnal *an) {

  cout << "############# Results from PiP0 Normalization Analysis #############" << endl;
}


void AnalysisWrapper::PNN(PNNAnal *an) {

  cout << "############# Results from PNN Analysis #############" << endl;

}
void AnalysisWrapper::KMU2CALO(Kmu2CaloAnal *an) {

  cout << "############# Results from KMU2 Calo Analysis #############" << endl;

}

///////////////

void AnalysisWrapper::PrintMomentumBins(double vmin, double vmax, double step) {
  int jstep = 0;
  double pval = vmin;
  while(pval<vmax) {
    pval+=step;
    printf("  [%6.3f,%6.3f] GeV/c | ",pval-step,pval);
    jstep++;
  }
  cout << endl;
}

void AnalysisWrapper::PrintEfficiencyInMomentum(double vmin, double vmax, double step, double fact, TEfficiency *eff) {
  int jstep = 0;
  double pval = vmin;
  while(pval<vmax) {
    pval+=step;
    printf("  %6.3f +%6.3f-%6.3f | ",fact*eff->GetEfficiency(jstep+1),fact*eff->GetEfficiencyErrorLow(jstep+1),fact*eff->GetEfficiencyErrorUp(jstep+1));
    jstep++;
  }
  printf( " | x%5.4f \n",1./fact);
}

void AnalysisWrapper::PrintResultInMomentum(NA62Parameter *par, TH1F *h1, TH1F *h2, TH1F *htot, TString name, double fact) {
  h1->Write();
  h2->Write();
  htot->Write();
  TH1F *h12 = (TH1F*)h1->Clone("h12");
  h12->Add(h2);
  TEfficiency *e12 = new TEfficiency(*h12,*htot);
  TEfficiency *e1 = new TEfficiency(*h1,*htot);
  TEfficiency *e2 = new TEfficiency(*h2,*htot);
  PrintEfficiencyInMomentum(par->Get("pmin"),par->Get("pmax"),par->Get("pstep"),fact,e12);
  PrintEfficiencyInMomentum(par->Get("pmin"),par->Get("pmax"),par->Get("pstep"),fact,e1);
  PrintEfficiencyInMomentum(par->Get("pmin"),par->Get("pmax"),par->Get("pstep"),fact,e2);
  e12->Write(Form("%sVSP_r12",name.Data()));
  e1->Write(Form("%sVSP_r1",name.Data()));
  e2->Write(Form("%sVSP_r2",name.Data()));
}

void AnalysisWrapper::PrintResultInMomentum(NA62Parameter *par, TH1F *h1, TH1F *htot, TString name, double fact) {
  h1->Write();
  htot->Write();
  TEfficiency *e1 = new TEfficiency(*h1,*htot);
  PrintEfficiencyInMomentum(par->Get("pmin"),par->Get("pmax"),par->Get("pstep"),fact,e1);
  e1->Write(Form("%sVSP",name.Data()));
}

////////////

void AnalysisWrapper::PrintIntensityBins(double *vmin, double *vmax, int nbins) {
  int jstep = 0;
  while(jstep<nbins) {
    printf("  [%6.3f,%6.3f]       | ",vmin[jstep],vmax[jstep]);
    jstep++;
  }
  cout << endl;
}

void AnalysisWrapper::PrintEfficiencyInIntensity(int nbins, double fact, TEfficiency *eff) {
  int jstep = 0;
  while(jstep<nbins) {
    printf("  %6.3f +%6.3f-%6.3f | ",fact*eff->GetEfficiency(jstep+1),fact*eff->GetEfficiencyErrorLow(jstep+1),fact*eff->GetEfficiencyErrorUp(jstep+1));
    jstep++;
  }
  printf( " | x%5.4f \n",1./fact);
}

void AnalysisWrapper::PrintResultInIntensity(int nbins, TH1F *h1, TH1F *h2, TH1F *htot, TString name, double fact) {
  h1->Write();
  h2->Write();
  htot->Write();
  TH1F *h12 = (TH1F*)h1->Clone("h12");
  h12->Add(h2);
  TEfficiency *e12 = new TEfficiency(*h12,*htot);
  TEfficiency *e1 = new TEfficiency(*h1,*htot);
  TEfficiency *e2 = new TEfficiency(*h2,*htot);
  PrintEfficiencyInIntensity(nbins,fact,e12);
  PrintEfficiencyInIntensity(nbins,fact,e1);
  PrintEfficiencyInIntensity(nbins,fact,e2);
  e12->Write(Form("%sVSI_r12",name.Data()));
  e1->Write(Form("%sVSI_r1",name.Data()));
  e2->Write(Form("%sVSI_r2",name.Data()));
}

void AnalysisWrapper::PrintResultInIntensity(int nbins, TH1F *h1, TH1F *htot, TString name, double fact) {
  h1->Write();
  htot->Write();
  TEfficiency *e1 = new TEfficiency(*h1,*htot);
  PrintEfficiencyInIntensity(nbins,fact,e1);
  e1->Write(Form("%sVSI",name.Data()));
}



//////////////////////////////
