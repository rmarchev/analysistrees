#include "PMINNNAnal.hh"

PMINNNAnal::PMINNNAnal() {

  DiscriminantNormalization();
}

PMINNNAnal::~PMINNNAnal() {
}

void PMINNNAnal::SetParameter() {

}

void PMINNNAnal::InitCounter() {

}
void PMINNNAnal::SetupVariables() {

  ResetEvent();

  fevt_  = GetEvent();
  fpion_ = (PnnNA62DownstreamParticle *)fevt_->GetDownstreamParticle();
  fkaon_ = (PnnNA62UpstreamParticle *)fevt_->GetUpstreamParticle();
  ftrig_ = (PnnNA62Trigger *)fevt_->GetTrigger();

  //Kinematic variables + instantaneous intensity
  fvtx_      = fevt_->GetVertex();
  fmm2_      = fevt_->GetMMiss();
  fmm2_rich_ = fevt_->GetMMissRich();
  fmm2_nomi_ = fevt_->GetMMissNominalK();
  fp_        = fpion_->GetP().Mag();
  ftrim5_x_  = fpion_->GetPosTrim5X();
  ftrim5_y_  = fpion_->GetPosTrim5Y();
  ftrim5_r_  = sqrt(pow(ftrim5_x_,2) + pow(ftrim5_y_, 2));
  fstraw1_x_ = fpion_->GetPosStraw1X();
  fstraw1_y_ = fpion_->GetPosStraw1Y();
  fstraw1_r_ = sqrt( pow(fstraw1_x_ - 101.2, 2) + pow(fstraw1_y_, 2));
  flambda_   = fevt_->GetLambda();
  fcda_      = fevt_->GetCDA();
  fbox_cut_  = fabs(ftrim5_x_)<100. && fabs(ftrim5_y_)<500.;
  //GTK timing variables and related cuts
  if ((fevt_->GetIsKTAGBeamDiscriminant()>0.03 || fevt_->GetIsRICHBeamDiscriminant()>0.03) && fevt_->GetIsKTAGBeamDiscriminant() > 0.005 && fevt_->GetIsRICHBeamDiscriminant() > 0.005)
      fgtk_match_ = true;

  ftgtk_     = (fkaon_->GetTGTK1()+fkaon_->GetTGTK2()+fkaon_->GetTGTK3())/3.;
  fdt_kk_    = fkaon_->GetTKTAG() - ftgtk_;
  fdt_kpi_   = fkaon_->GetTKTAG() - fpion_->GetTRICH();
  fdt13_     = fkaon_->GetTGTK1() - fkaon_->GetTGTK3();
  fdt23_     = fkaon_->GetTGTK2() - fkaon_->GetTGTK3();
  fdt12_     = fkaon_->GetTGTK1() - fkaon_->GetTGTK2();
  fellipse_  = fdt_kk_*fdt_kk_/(2.5*0.13*2.5*0.13) + fdt_kpi_*fdt_kpi_/(2.5*0.141*2.5*0.141) < 1 ;

  fd13_      = Discriminant(fdt13_);
  fd23_      = Discriminant(fdt23_);
  fd12_      = Discriminant(fdt12_);
  fgtk_cut_   = (fkaon_->GetNGTKTracks()==1 && fd13_ > 0.3 && fd23_ > 0.3 && fd12_ > 0.3) || (fkaon_->GetNGTKTracks()==2 && fd13_ > 0.4 && fd23_ > 0.4 && fd12_ > 0.4) || (fkaon_->GetNGTKTracks()> 2 && fd13_ > 0.9 && fd23_ > 0.9 && fd12_ > 0.9);

  //PID variables
  for (int j=0; j<5; j++) {
    if (j==3) continue;
    if (fpion_->GetRICHLikeProb(j)> fmax_lh_) fmax_lh_ = fpion_->GetRICHLikeProb(j);
  }
  frich_mass_range_ = fpion_->GetRICHMass()>0.125&&fpion_->GetRICHMass()<0.2 ? true : false;
  if (fmax_lh_<=0.12 && frich_mass_range_) frich_pid_ = true;

  fcalo_pid_    = PionPID();

  //Beam background variables
  fsnake_       = IsSnake();
  fbb_cut_      = fsnake_ || fevt_->GetIsSignalInCHANTI() || fevt_->GetIsInteraction() || fkaon_->GetNGTKTracks()>5 || fvtx_.Z() <= 105000 || fvtx_.Z() > 165000;


  //Region definitions
  fpipi_region_ = fmm2_ <= 0.021 && fmm2_ > 0.015;
  fp_region_    = fp_ <= 35 && fp_ > 15;
  fregion_      = GetRegion();

  //PhotonRejection
  fphoton_      = fevt_->GetIsSignalInLAV() || fevt_->GetIsSignalInSAC() || fevt_->GetIsSignalInIRC() || fevt_->GetIsSignalInSAV() || fevt_->GetIsPhotonInLKr() || fevt_->GetIsAuxiliaryLKr()|| fevt_->GetIsLKrInTimeHits();
  fhit_mult_    = fevt_->GetMultAllOldCH7() > 3 ||fevt_->GetMultOldNewCH() > 0 || fevt_->GetMultNewCHLKr() > 0 || fevt_->GetMultOldCHLKr() > 0 || fevt_->GetMultHAC() == 1 || fevt_->GetMultMUV0() == 1 || (fevt_->GetMultLKrMerged() && fevt_->GetMultAllOldCH7() > 0);
  fmult_tot_    = fevt_->GetIsMultiplicity() || fevt_->GetIsSegment() || !fevt_->GetIsOneParticle() || fevt_->GetIsBroadMultiVertex();

  return;
}

void PMINNNAnal::ResetEvent() {

  fevt_  = NULL;
  fpion_ = NULL;
  fkaon_ = NULL;
  ftrig_ = NULL;

  fvtx_      = TVector3(0.,0.,0.);
  fmm2_      = -99999;
  fmm2_rich_ = -99999;
  fmm2_nomi_ = -99999;
  fp_        = -99999;
  ftrim5_x_  = -99999;
  ftrim5_y_  = -99999;
  ftrim5_r_  = -99999;
  flambda_   = -99999;
  fcda_      = -99999;

  fgtk_match_ = 0.;
  ftgtk_     = -99999;
  fdt_kk_    = -99999;
  fdt_kpi_   = -99999;
  fellipse_  = 0;
  fdt13_     = -99999;
  fdt23_     = -99999;
  fdt12_     = -99999;
  fd13_      = -99999;
  fd23_      = -99999;
  fd12_      = -99999;
  fgtk_cut_  = 0.;

  fcalo_pid_ = 0;
  frich_pid_ = 0;
  fmax_lh_   = -99999;
  frich_mass_range_ = 0;
  fsnake_    = 0;
  fpipi_region_ = 0;
  fp_region_ = 0;
  fbb_cut_   = 0;
  fphoton_   = 0;
  fhit_mult_ = 0;
  fmult_tot_ = 0;
  fbox_cut_  = 0;
}
/////////////////////////
//// USER ANALYSIS //////
/////////////////////////

bool PMINNNAnal::EventAnalysis() {
  SetupVariables();

  FullPinunuAnalysis();
  UpstreamBackgroundAnalysis();
  return 1;
}

///////
//// -> Add Segment !!!
int PMINNNAnal::FullPinunuAnalysis() {

  fHisto->FillHisto("PMIN_mmiss_vs_p_0",fp_,fmm2_);
  fHisto->FillHisto("PMIN_mmiss_vs_mmissRICH_0",fmm2_,fmm2_rich_);
  fHisto->FillHisto("PMIN_mmiss_vs_mmissNominalK_0",fmm2_,fmm2_nomi_);
  fHisto->FillHisto("PMIN_mmiss_vs_zvtx_0",fvtx_.Z(), fmm2_);
  fHisto->FillHisto("PMIN_p_vs_zvtx_0",fvtx_.Z(), fp_);
  fHisto->FillHisto("PMIN_cda_vs_dt_0",fdt_kk_,fcda_);
  fHisto->FillHisto("PMIN_dt13_vs_dt12_0",fdt12_,fdt13_);
  fHisto->FillHisto("PMIN_dt12_vs_dt23_0",fdt12_,fdt23_);
  fHisto->FillHisto("PMIN_dt12_vs_dt13_0",fdt12_,fdt13_);
  fHisto->FillHisto("PMIN_dt13_vs_dt23_0",fdt13_,fdt23_);

  //Photon and hit multiplicity rejection
  if(fphoton_ || fhit_mult_) return 0;
  if(fabs(fkaon_->GetTGTK()-fpion_->GetTCHOD()) > 1.1) return 0;
  if(fabs(fdt_kk_) > 0.6) return 0;
  // BeamBackgroundCut
  if(fbb_cut_) return 0;
  fHisto->FillHisto("PMIN_mmiss_vs_p_1",fp_,fmm2_);
  fHisto->FillHisto("PMIN_mmiss_vs_mmissRICH_1",fmm2_,fmm2_rich_);
  fHisto->FillHisto("PMIN_mmiss_vs_mmissNominalK_1",fmm2_,fmm2_nomi_);
  fHisto->FillHisto("PMIN_mmiss_vs_zvtx_1",fvtx_.Z(), fmm2_);
  fHisto->FillHisto("PMIN_p_vs_zvtx_1",fvtx_.Z(), fp_);
  fHisto->FillHisto("PMIN_cda_vs_dt_1",fdt_kk_,fcda_);
  fHisto->FillHisto("PMIN_downstream_distance_1",fpion_->GetDownstreamDistance());

  if (fevt_->GetIsBroadMultiVertex()) return 0;

  fHisto->FillHisto("PMIN_mmiss_vs_p_2",fp_,fmm2_);
  fHisto->FillHisto("PMIN_mmiss_vs_mmissRICH_2",fmm2_,fmm2_rich_);
  fHisto->FillHisto("PMIN_mmiss_vs_mmissNominalK_2",fmm2_,fmm2_nomi_);
  fHisto->FillHisto("PMIN_mmiss_vs_zvtx_2",fvtx_.Z(), fmm2_);
  fHisto->FillHisto("PMIN_p_vs_zvtx_2",fvtx_.Z(), fp_);
  fHisto->FillHisto("PMIN_cda_vs_dt_2",fdt_kk_,fcda_);
  fHisto->FillHisto("PMIN_downstream_distance_2",fpion_->GetDownstreamDistance());

  if (fbox_cut_) return 0;
  if(fkaon_->GetGTKExtraHits()) return 0;
  if (!fcalo_pid_) return 0;

  fHisto->FillHisto("PMIN_mmiss_vs_p_3",fp_,fmm2_);
  fHisto->FillHisto("PMIN_mmiss_vs_mmissRICH_3",fmm2_,fmm2_rich_);
  fHisto->FillHisto("PMIN_mmiss_vs_mmissNominalK_3",fmm2_,fmm2_nomi_);
  fHisto->FillHisto("PMIN_mmiss_vs_zvtx_3",fvtx_.Z(), fmm2_);
  fHisto->FillHisto("PMIN_p_vs_zvtx_3",fvtx_.Z(), fp_);
  fHisto->FillHisto("PMIN_cda_vs_dt_3",fdt_kk_,fcda_);
  fHisto->FillHisto("PMIN_downstream_distance_3",fpion_->GetDownstreamDistance());

  fHisto->FillHisto("PMIN_lambda_tot",flambda_);

  bool RKE4 = fp_ > 15 && fp_ <= 35 && fmm2_ <= 0.068 && fmm2_ > 0.03;

  if(RKE4){
    fHisto->FillHisto("PMIN_lambda",flambda_);
    // if (frich_pid_ && fgtk_match_ && fellipse_) fHisto->FillHisto("PMIN_RICH_lambda",flambda_);
    if (frich_pid_ && fgtk_match_) fHisto->FillHisto("PMIN_RICH_lambda",flambda_);
  }

  if (fevt_->GetIsSegment() || !fevt_->GetIsOneParticle()){

    if(RKE4)
      fHisto->FillHisto("PMIN_BIF_lambda_4",flambda_);

    fHisto->FillHisto("PMIN_BIF_mmiss_vs_p_4",fp_,fmm2_);
    fHisto->FillHisto("PMIN_BIF_mmiss_vs_mmissRICH_4",fmm2_,fmm2_rich_);
    fHisto->FillHisto("PMIN_BIF_mmiss_vs_mmissNominalK_4",fmm2_,fmm2_nomi_);
    fHisto->FillHisto("PMIN_BIF_mmiss_vs_zvtx_4",fvtx_.Z(), fmm2_);
    fHisto->FillHisto("PMIN_BIF_p_vs_zvtx_4",fvtx_.Z(), fp_);
    fHisto->FillHisto("PMIN_BIF_cda_vs_dt_4",fdt_kk_,fcda_);
    fHisto->FillHisto("PMIN_BIF_downstream_distance_4",fpion_->GetDownstreamDistance());

    // if (frich_pid_ && fgtk_match_ && fellipse_){
    if (frich_pid_ && fgtk_match_){
      if(RKE4)
        fHisto->FillHisto("PMIN_BIFRICH_lambda_4",flambda_);
      fHisto->FillHisto("PMIN_BIFRICH_mmiss_vs_p_4",fp_,fmm2_);
      fHisto->FillHisto("PMIN_BIFRICH_mmiss_vs_mmissRICH_4",fmm2_,fmm2_rich_);
      fHisto->FillHisto("PMIN_BIFRICH_mmiss_vs_mmissNominalK_4",fmm2_,fmm2_nomi_);
      fHisto->FillHisto("PMIN_BIFRICH_mmiss_vs_zvtx_4",fvtx_.Z(), fmm2_);
      fHisto->FillHisto("PMIN_BIFRICH_p_vs_zvtx_4",fvtx_.Z(), fp_);
      fHisto->FillHisto("PMIN_BIFRICH_cda_vs_dt_4",fdt_kk_,fcda_);
      fHisto->FillHisto("PMIN_BIFRICH_downstream_distance_4",fpion_->GetDownstreamDistance());

    }
    return 0;
  }


  if(RKE4)
    fHisto->FillHisto("PMIN_PNN_lambda_5",flambda_);

  fHisto->FillHisto("PMIN_PNN_mmiss_vs_p_5",fp_,fmm2_);
  fHisto->FillHisto("PMIN_PNN_mmiss_vs_mmissRICH_5",fmm2_,fmm2_rich_);
  fHisto->FillHisto("PMIN_PNN_mmiss_vs_mmissNominalK_5",fmm2_,fmm2_nomi_);
  fHisto->FillHisto("PMIN_PNN_mmiss_vs_zvtx_5",fvtx_.Z(), fmm2_);
  fHisto->FillHisto("PMIN_PNN_p_vs_zvtx_5",fvtx_.Z(), fp_);
  fHisto->FillHisto("PMIN_PNN_cda_vs_dt_5",fdt_kk_,fcda_);
  fHisto->FillHisto("PMIN_PNN_downstream_distance_5",fpion_->GetDownstreamDistance());

  // if (!frich_pid_ || !fgtk_match_ || !fellipse_) return 0;
  if (!frich_pid_ || !fgtk_match_ ) return 0;
  if(RKE4)
    fHisto->FillHisto("PMIN_PNNRICH_lambda_5",flambda_);
  fHisto->FillHisto("PMIN_PNNRICH_mmiss_vs_p_5",fp_,fmm2_);
  fHisto->FillHisto("PMIN_PNNRICH_mmiss_vs_mmissRICH_5",fmm2_,fmm2_rich_);
  fHisto->FillHisto("PMIN_PNNRICH_mmiss_vs_mmissNominalK_5",fmm2_,fmm2_nomi_);
  fHisto->FillHisto("PMIN_PNNRICH_mmiss_vs_zvtx_5",fvtx_.Z(), fmm2_);
  fHisto->FillHisto("PMIN_PNNRICH_p_vs_zvtx_5",fvtx_.Z(), fp_);
  fHisto->FillHisto("PMIN_PNNRICH_cda_vs_dt_5",fdt_kk_,fcda_);
  fHisto->FillHisto("PMIN_PNNRICH_downstream_distance_5",fpion_->GetDownstreamDistance());

  //if((int)fevt_->GetTimestamp()% 256 < 35 && (int)fevt_->GetTimestamp()%256 > 28) return 0;


  fHisto->FillHisto("PMIN_PNNRICH_mmiss_vs_p_6",fp_,fmm2_);
  fHisto->FillHisto("Region_vs_GTKExtra", fkaon_->GetGTKExtraHits(), fregion_);
  return 1;
}

int PMINNNAnal::UpstreamBackgroundAnalysis() {

  //Regions for the bifurcation study
  bool A =  fgtk_match_ && !fbox_cut_; // signal region: kpi match & outbox
  bool B =  fgtk_match_ &&  fbox_cut_; // Region B kpi match & inbox
  bool C = !fgtk_match_ && !fbox_cut_; // Region C nokpi match & outbox
  bool D = !fgtk_match_ &&  fbox_cut_; // Region D nokpi match & inbox

  TLorentzVector p4pi, p4ks;
  p4pi.SetXYZM(fpion_->GetP().X(), fpion_->GetP().Y(), fpion_->GetP().Z(), 0.13957018);
  p4ks.SetXYZM(fkaon_->GetP().X(), fkaon_->GetP().Y(), fkaon_->GetP().Z(), 0.497614); //KS mass
  double mm2_ks = (p4ks - p4pi).Mag2();

  //bb enriched selection
  bool bbenriched = !fgtk_match_&&!fmult_tot_&&!fphoton_&&frich_pid_&&fcalo_pid_;

  if(bbenriched){

    fHisto->FillHisto("PMIN_Mamba_mmiss_vs_p_0", fp_ ,fmm2_);
    fHisto->FillHisto("PMIN_Mamba_mmissks_vs_p_0", fp_ ,mm2_ks);
    fHisto->FillHisto("PMIN_Mamba_y_vs_x_trim5_0", ftrim5_x_, ftrim5_y_);
    fHisto->FillHisto("PMIN_Mamba_cda_vs_dt_0", fdt_kk_,fcda_);
    fHisto->FillHisto("PMIN_Mamba_rtrim5_vs_zvtx_0", fvtx_.Z(),ftrim5_r_);
    fHisto->FillHisto("PMIN_Mamba_rstraw1_vs_zvtx_0", fvtx_.Z(),fstraw1_r_);
    fHisto->FillHisto("PMIN_Mamba_dtkpi_vs_dtkk_0", fdt_kk_, fdt_kpi_);
    fHisto->FillHisto("PMIN_Mamba_mrich_vs_p_0", fp_, fpion_->GetRICHMass());
    fHisto->FillHisto("PMIN_Mamba_dtij_0", fgtk_cut_);
    fHisto->FillHisto("PMIN_Mamba_downstream_dist_0", fpion_->GetDownstreamDistance());
    fHisto->FillHisto("PMIN_Mamba_lambda_0", flambda_);
    fHisto->FillHisto("PMIN_Mamba_NGTKCandidates_0",fkaon_->GetNGTKTracks());

    if(fp_region_&&(fregion_ == 1 || fregion_ == 2)){
      fHisto->FillHisto("PMIN_Mamba_mmiss_vs_p_0R12", fp_ ,fmm2_);
      fHisto->FillHisto("PMIN_Mamba_mmissks_vs_p_0R12", fp_ ,mm2_ks);
      fHisto->FillHisto("PMIN_Mamba_y_vs_x_trim5_0R12", ftrim5_x_, ftrim5_y_);
      fHisto->FillHisto("PMIN_Mamba_cda_vs_dt_0R12", fdt_kk_,fcda_);
      fHisto->FillHisto("PMIN_Mamba_rtrim5_vs_zvtx_0R12", fvtx_.Z(),ftrim5_r_);
      fHisto->FillHisto("PMIN_Mamba_rstraw1_vs_zvtx_0R12", fvtx_.Z(),fstraw1_r_);
      fHisto->FillHisto("PMIN_Mamba_dtkpi_vs_dtkk_0R12", fdt_kk_, fdt_kpi_);
      fHisto->FillHisto("PMIN_Mamba_mrich_vs_p_0R12", fp_, fpion_->GetRICHMass());
      fHisto->FillHisto("PMIN_Mamba_dtij_0R12", fgtk_cut_);
      fHisto->FillHisto("PMIN_Mamba_downstream_dist_0R12", fpion_->GetDownstreamDistance());
      fHisto->FillHisto("PMIN_Mamba_lambda_0R12", flambda_);
      fHisto->FillHisto("PMIN_Mamba_NGTKCandidates_0R12",fkaon_->GetNGTKTracks());
    }

  }

  //Apply pnn selection
  if(fbb_cut_)    return 0; //Beam background cut
  if(fphoton_)    return 0; //Photon rejection cluster-based
  if(fmult_tot_)  return 0; //Complete set of hit multiplicity cuts
  if(!fcalo_pid_) return 0; //CALO PID
  if(!frich_pid_) return 0; //RICH PID
  if(!fp_region_) return 0; //15-35GeV/c cut

  if(fregion_!=1 && fregion_!=2 && fregion_!=400) return 0;
  if(fabs(fkaon_->GetTGTK()-fpion_->GetTCHOD()) > 1.1) return 0;
  if(fabs(fdt_kk_) > 0.6) return 0;
  //if(!fgtk_cut_ || !fellipse_) return 0;

  //cout <<fevt_->GetIsKTAGBeamDiscriminant() << " " << fevt_->GetIsRICHBeamDiscriminant() <<  " gtkm = " << fgtk_match_ << " bc = " << fbox_cut_ << " "  << ftrim5_x_ << " " << ftrim5_y_ << endl;
  //cout << A << "  " << B << " " << C << "  " << D << " " << endl;
  fHisto->FillHisto("PMIN_Mamba_mmiss_vs_p_1", fp_ ,fmm2_);
  fHisto->FillHisto("PMIN_Mamba_mmissks_vs_p_1", fp_ ,mm2_ks);
  fHisto->FillHisto("PMIN_Mamba_y_vs_x_trim5_1", ftrim5_x_, ftrim5_y_);
  fHisto->FillHisto("PMIN_Mamba_cda_vs_dt_1", fdt_kk_,fcda_);
  fHisto->FillHisto("PMIN_Mamba_rtrim5_vs_zvtx_1", fvtx_.Z(),ftrim5_r_);
  fHisto->FillHisto("PMIN_Mamba_rstraw1_vs_zvtx_1", fvtx_.Z(),fstraw1_r_);
  fHisto->FillHisto("PMIN_Mamba_dtkpi_vs_dtkk_1", fdt_kk_, fdt_kpi_);
  fHisto->FillHisto("PMIN_Mamba_mrich_vs_p_1", fp_, fpion_->GetRICHMass());
  fHisto->FillHisto("PMIN_Mamba_dtij_1", fgtk_cut_);
  fHisto->FillHisto("PMIN_Mamba_downstream_dist_1", fpion_->GetDownstreamDistance());
  fHisto->FillHisto("PMIN_Mamba_lambda_1", flambda_);
  fHisto->FillHisto("PMIN_Mamba_NGTKCandidates_1",fkaon_->GetNGTKTracks());

  if(A){
    fHisto->FillHisto("PMIN_Mamba_mmiss_vs_p_A", fp_ ,fmm2_);
    fHisto->FillHisto("PMIN_Mamba_mmissks_vs_p_A", fp_ ,mm2_ks);
    fHisto->FillHisto("PMIN_Mamba_y_vs_x_trim5_A", ftrim5_x_, ftrim5_y_);
    fHisto->FillHisto("PMIN_Mamba_cda_vs_dt_A", fdt_kk_,fcda_);
    fHisto->FillHisto("PMIN_Mamba_rtrim5_vs_zvtx_A", fvtx_.Z(),ftrim5_r_);
    fHisto->FillHisto("PMIN_Mamba_rstraw1_vs_zvtx_A", fvtx_.Z(),fstraw1_r_);
    fHisto->FillHisto("PMIN_Mamba_dtkpi_vs_dtkk_A", fdt_kk_, fdt_kpi_);
    fHisto->FillHisto("PMIN_Mamba_mrich_vs_p_A", fp_, fpion_->GetRICHMass());
    fHisto->FillHisto("PMIN_Mamba_dtij_A", fgtk_cut_);
    fHisto->FillHisto("PMIN_Mamba_downstream_dist_A", fpion_->GetDownstreamDistance());
    fHisto->FillHisto("PMIN_Mamba_lambda_A", flambda_);
    fHisto->FillHisto("PMIN_Mamba_NGTKCandidates_A",fkaon_->GetNGTKTracks());
  }

  if(B){
    fHisto->FillHisto("PMIN_Mamba_mmiss_vs_p_B", fp_ ,fmm2_);
    fHisto->FillHisto("PMIN_Mamba_mmissks_vs_p_B", fp_ ,mm2_ks);
    fHisto->FillHisto("PMIN_Mamba_y_vs_x_trim5_B", ftrim5_x_, ftrim5_y_);
    fHisto->FillHisto("PMIN_Mamba_cda_vs_dt_B", fdt_kk_,fcda_);
    fHisto->FillHisto("PMIN_Mamba_rtrim5_vs_zvtx_B", fvtx_.Z(),ftrim5_r_);
    fHisto->FillHisto("PMIN_Mamba_rstraw1_vs_zvtx_B", fvtx_.Z(),fstraw1_r_);
    fHisto->FillHisto("PMIN_Mamba_dtkpi_vs_dtkk_B", fdt_kk_, fdt_kpi_);
    fHisto->FillHisto("PMIN_Mamba_mrich_vs_p_B", fp_, fpion_->GetRICHMass());
    fHisto->FillHisto("PMIN_Mamba_dtij_B", fgtk_cut_);
    fHisto->FillHisto("PMIN_Mamba_downstream_dist_B", fpion_->GetDownstreamDistance());
    fHisto->FillHisto("PMIN_Mamba_lambda_B", flambda_);
    fHisto->FillHisto("PMIN_Mamba_NGTKCandidates_B",fkaon_->GetNGTKTracks());
  }

  if(C){
    fHisto->FillHisto("PMIN_Mamba_mmiss_vs_p_C", fp_ ,fmm2_);
    fHisto->FillHisto("PMIN_Mamba_mmissks_vs_p_C", fp_ ,mm2_ks);
    fHisto->FillHisto("PMIN_Mamba_y_vs_x_trim5_C", ftrim5_x_, ftrim5_y_);
    fHisto->FillHisto("PMIN_Mamba_cda_vs_dt_C", fdt_kk_,fcda_);
    fHisto->FillHisto("PMIN_Mamba_rtrim5_vs_zvtx_C", fvtx_.Z(),ftrim5_r_);
    fHisto->FillHisto("PMIN_Mamba_rstraw1_vs_zvtx_C", fvtx_.Z(),fstraw1_r_);
    fHisto->FillHisto("PMIN_Mamba_dtkpi_vs_dtkk_C", fdt_kk_, fdt_kpi_);
    fHisto->FillHisto("PMIN_Mamba_mrich_vs_p_C", fp_, fpion_->GetRICHMass());
    fHisto->FillHisto("PMIN_Mamba_dtij_C", fgtk_cut_);
    fHisto->FillHisto("PMIN_Mamba_downstream_dist_C", fpion_->GetDownstreamDistance());
    fHisto->FillHisto("PMIN_Mamba_lambda_C", flambda_);
    fHisto->FillHisto("PMIN_Mamba_NGTKCandidates_C",fkaon_->GetNGTKTracks());
  }

  if(D){
    fHisto->FillHisto("PMIN_Mamba_mmiss_vs_p_D", fp_ ,fmm2_);
    fHisto->FillHisto("PMIN_Mamba_mmissks_vs_p_D", fp_ ,mm2_ks);
    fHisto->FillHisto("PMIN_Mamba_y_vs_x_trim5_D", ftrim5_x_, ftrim5_y_);
    fHisto->FillHisto("PMIN_Mamba_cda_vs_dt_D", fdt_kk_,fcda_);
    fHisto->FillHisto("PMIN_Mamba_rtrim5_vs_zvtx_D", fvtx_.Z(),ftrim5_r_);
    fHisto->FillHisto("PMIN_Mamba_rstraw1_vs_zvtx_D", fvtx_.Z(),fstraw1_r_);
    fHisto->FillHisto("PMIN_Mamba_dtkpi_vs_dtkk_D", fdt_kk_, fdt_kpi_);
    fHisto->FillHisto("PMIN_Mamba_mrich_vs_p_D", fp_, fpion_->GetRICHMass());
    fHisto->FillHisto("PMIN_Mamba_dtij_D", fgtk_cut_);
    fHisto->FillHisto("PMIN_Mamba_downstream_dist_D", fpion_->GetDownstreamDistance());
    fHisto->FillHisto("PMIN_Mamba_lambda_D", flambda_);
    fHisto->FillHisto("PMIN_Mamba_NGTKCandidates_D",fkaon_->GetNGTKTracks());
  }

  return 1;
}

void PMINNNAnal::DiscriminantNormalization() {

  double pass_dt = 0.001; // mm

  fPDFKaonDT_.clear();
  fIntPDFKaonDT_ = 0;
  for (int jbindt(0); jbindt<3000; jbindt++) { // +-3 ns
    double dt = pass_dt*jbindt+0.5*pass_dt;
    fIntPDFKaonDT_ += (PDFKaonDT(dt)+PDFKaonDT(-dt))*pass_dt/0.01; // compute the total integral of the PDF
    fPDFKaonDT_.push_back(fIntPDFKaonDT_); // fill the cumulative distribution function
  }
}

Double_t PMINNNAnal::Discriminant(const Double_t &dt_val) {
  double D = 0;

  if(fabs(dt_val) >= 3) return D;

  double pkdt    = 0;
  double pass_dt = 0.001;
  for (auto ikdt = fPDFKaonDT_.begin(); ikdt != fPDFKaonDT_.end();++ikdt) {
    double id = (double)(distance(fPDFKaonDT_.begin(),ikdt))*pass_dt; // compute the time corresponding to each element of the cumulative distribution function
    if(fabs(dt_val) < id) {
      pkdt = id/fIntPDFKaonDT_;
      //cout << *ikdt << "  " <<  distance(fPDFKaonDT.begin(),ikdt) << "  " << id <<  " " << fabs(dt_val) << " " <<  pkdt <<  endl;

      break;
    }
  }

  D = (1 - pkdt);
  return D;
}
Double_t PMINNNAnal::PDFKaonDT(Double_t dt) {
  Double_t dtpar[3];

  dtpar[0] = 0.02;
  dtpar[1] = 0.;
  dtpar[2] = 0.2;

  Double_t dtg1 = dtpar[0]*exp(-0.5*((dt-dtpar[1])/dtpar[2])*((dt-dtpar[1])/dtpar[2]));
  return dtg1;
}

int PMINNNAnal::PionPID() {

  bool D = 0;

  if (fpion_->GetIsMUV3()) D = 1;
  if (fpion_->GetIsMulti()) D = 1;
  if (fpion_->GetIsMip()) D = 1;

  Double_t caloEnergy = fpion_->GetCaloEnergy();
  Double_t r1 = fpion_->GetMUV1Energy()/caloEnergy;
  Double_t r2 = fpion_->GetMUV2Energy()/caloEnergy;
  Double_t elkr = fpion_->GetLKrEnergy();
  Double_t seedratio = fpion_->GetLKrSeedEnergy()/elkr;
  Double_t cellratio = fpion_->GetLKrNCells()/elkr;
  bool isLKrEM = 0;
  auto vcut       = TMath::Max(0.7,0.98-0.4596*exp(-(fp_-11.44)/5.27));
  bool IsCaloPion = fpion_->GetCaloPionProb()>=vcut;

  //if(flag == 1)

  //cout << " Fp_ =- " << fp_ << " elkr =- " << elkr << " " << vcut << "  " << r1 << "  " << r2 << "  " << seedratio << "  " << cellratio << " Ecal = " << caloEnergy << " m1 = " << fpion_->GetMUV1Energy() << " m2 = " << fpion_->GetMUV2Energy() << endl;

  if (r1<0.01 && r2<0.01) {
    if (seedratio>0.2&&cellratio<=3) isLKrEM = 1;
    if (seedratio<=0.2&&cellratio<1.8) isLKrEM = 1;
    if (seedratio>0.35) isLKrEM = 1;
    if (seedratio<0.05) isLKrEM = 1;
  }
  if (seedratio>0. && seedratio<0.8 && cellratio<1.4) isLKrEM = 1;

  if(!IsCaloPion) D = 1;
  if (isLKrEM) D = 1;
  if (caloEnergy>1.2*fp_) D = 1;
  if (elkr>0.8*fp_) D = 1;
  if (!fpion_->GetIsGoodMUV1() && fpion_->GetIsGoodMUV2()) D = 1;

  return D > 0 ? 0 : 1;
}


bool PMINNNAnal::IsSnake() {
  PnnNA62Event *evt = GetEvent();
  PnnNA62DownstreamParticle *pion = (PnnNA62DownstreamParticle *)evt->GetDownstreamParticle();
  double zvtx = evt->GetVertex().Z();
  double rst1 = sqrt((pion->GetPosStraw1X()-101.2)*(pion->GetPosStraw1X()-101.2)+pion->GetPosStraw1Y()*pion->GetPosStraw1Y());
  double b1 = -0.004;
  double a1 = 315-b1*105000.;
  double r115 = a1+b1*115000.;
  double b2 = -(900-r115)/10000.;
  double a2 = 900-b2*105000.;
  double b3 = -0.00983333333;
  double a3 = 780-b3*105000.;
  double cut1 = a1+b1*zvtx;
  double cut2 = a2+b2*zvtx;
  double cut3 = a3+b3*zvtx;
  bool isSignal = rst1>cut1 && rst1>cut2 && rst1<cut3 && (zvtx)<165000.;
  return !isSignal;
}

int PMINNNAnal::GetRegion() {
  bool isPNNRegion1 = 0;
  double bound = Kmu2KinematicBound(fp_);
  if (fp_<=20.)
    isPNNRegion1 = (fmm2_>0.    && fmm2_<=0.01)  && (fmm2_nomi_>-0.005&&fmm2_nomi_<=0.0135) && (fmm2_rich_>0.    &&fmm2_rich_<=0.01);
  if (fp_>20. && fp_<=25.)
    isPNNRegion1 = (fmm2_>0.    && fmm2_<=0.01)  && (fmm2_nomi_>-0.005&&fmm2_nomi_<=0.0135) && (fmm2_rich_>0.    &&fmm2_rich_<=0.02);
  if (fp_>25.)
    isPNNRegion1 = (fmm2_>0.    && fmm2_<=0.01)  && (fmm2_nomi_>0.    &&fmm2_nomi_<=0.0135) && (fmm2_rich_>-0.005&&fmm2_rich_<=0.02);

  bool isPNNRegion2 = (fmm2_>0.026 && fmm2_<=0.068) && (fmm2_nomi_>0.024 &&fmm2_nomi_<=0.068)  && (fmm2_rich_>0.02  &&fmm2_rich_<=0.07);
  bool isCR1        = fmm2_>0.01  && fmm2_<=0.015;
  bool isCR2        = fmm2_>0.021 && fmm2_<=0.026;
  bool isCRmu       = fmm2_<=0     && fmm2_>bound+0.0036;
  bool isCR3pi      = fmm2_>0.068 && fmm2_<=0.072;
  bool isCRUp       = (fmm2_>bound-0.0070 && fmm2_<=bound-0.0060);
  bool isK2piRegion = (fmm2_>0.015 && fmm2_<=0.021);
  bool isKmu2Region = /*(fmm2_>-0.05 && fmm2_<bound+0.0036)*/(fmm2_>bound-0.0060 && fmm2_<= bound+0.0036);
  bool isK3PiRegion  = (fmm2_>0.072);
  bool isUpRegion   = fmm2_<=bound-0.0070;

  if (isPNNRegion1) return 1;
  if (isPNNRegion2) return 2;
  if (isCR1) return 10;
  if (isCR2) return 20;
  if (isCRmu) return 30;
  if (isCR3pi) return 40;
  if (isCRUp) return 50;
  if (isK2piRegion) return 100;
  if (isKmu2Region) return 200;
  if (isK3PiRegion) return 300;
  if (isUpRegion) return 400;


  return 0;
}
double PMINNNAnal::Kmu2KinematicBound(double p){

  double mpi = 0.13957018;
  double mmu = 0.1056583745;

  return (pow(mpi,2) - pow(mmu,2))*(1 - 75/p);
}
