#include "Kmu2Anal.hh"

Kmu2Anal::Kmu2Anal(){
  DiscriminantNormalization();
}
Kmu2Anal::~Kmu2Anal(){

}

void Kmu2Anal::SetParameter() {
}
void Kmu2Anal::InitCounter() {
}
void Kmu2Anal::SetupVariables() {

  ResetEvent();

  fevt_  = GetEvent();
  fmuon_ = (PnnNA62DownstreamParticle *)fevt_->GetDownstreamParticle();
  fkaon_ = (PnnNA62UpstreamParticle *)fevt_->GetUpstreamParticle();
  ftrig_ = (PnnNA62Trigger *)fevt_->GetTrigger();

  //Kinematic variables + instantaneous intensity
  fvtx_      = fevt_->GetVertex();
  fmm2_      = fevt_->GetMMiss();
  fmm2_rich_ = fevt_->GetMMissRich();
  fmm2_nomi_ = fevt_->GetMMissNominalK();
  fp_        = fmuon_->GetP().Mag();
  ftrim5_x_  = fmuon_->GetPosTrim5X();
  ftrim5_y_  = fmuon_->GetPosTrim5Y();
  ftrim5_r_  = sqrt(pow(ftrim5_x_,2) + pow(ftrim5_y_, 2));
  fstraw1_x_ = fmuon_->GetPosStraw1X();
  fstraw1_y_ = fmuon_->GetPosStraw1Y();
  fstraw1_r_ = sqrt( pow(fstraw1_x_ - 101.2, 2) + pow(fstraw1_y_, 2));
  flambda_   = fevt_->GetLambda();
  fcda_      = fevt_->GetCDA();
  fbox_cut_  = fabs(ftrim5_x_)<100. && fabs(ftrim5_y_)<500.;

  //GTK timing variables and related cuts
  if ((fevt_->GetIsKTAGBeamDiscriminant()>0.03 || fevt_->GetIsRICHBeamDiscriminant()>0.03) && fevt_->GetIsKTAGBeamDiscriminant() > 0.005 && fevt_->GetIsRICHBeamDiscriminant() > 0.005)
  //if ((fevt_->GetIsKTAGBeamDiscriminant()>0.03 || fevt_->GetIsRICHBeamDiscriminant()>0.03) && fevt_->GetIsKTAGBeamDiscriminant() > 0.01 && fevt_->GetIsRICHBeamDiscriminant() > 0.01) // new working point for the discriminants
    fgtk_match_ = true;

  ftgtk_     = (fkaon_->GetTGTK1()+fkaon_->GetTGTK2()+fkaon_->GetTGTK3())/3.;
  fdt_kk_    = ftgtk_-fkaon_->GetTKTAG();
  fdt_kpi_   = fkaon_->GetTKTAG() - fmuon_->GetTRICH();
  fdt13_     = fkaon_->GetTGTK1() - fkaon_->GetTGTK3();
  fdt23_     = fkaon_->GetTGTK2() - fkaon_->GetTGTK3();
  fdt12_     = fkaon_->GetTGTK1() - fkaon_->GetTGTK2();
  fellipse_  = fdt_kk_*fdt_kk_/(2.5*0.13*2.5*0.13) + fdt_kpi_*fdt_kpi_/(2.5*0.141*2.5*0.141) < 1 ;

  fd13_      = Discriminant(fdt13_);
  fd23_      = Discriminant(fdt23_);
  fd12_      = Discriminant(fdt12_);
  fgtk_cut_  = (fkaon_->GetNGTKTracks()==1 && fd13_ > 0.3 && fd23_ > 0.3 && fd12_ > 0.3) || (fkaon_->GetNGTKTracks()==2 && fd13_ > 0.4 && fd23_ > 0.4 && fd12_ > 0.4) || (fkaon_->GetNGTKTracks()> 2 && fd13_ > 0.9 && fd23_ > 0.9 && fd12_ > 0.9);

  //RICH variables
  for (int j=0; j<5; j++) {
    if (j==3) continue;
    if (fmuon_->GetRICHLikeProb(j)> fmax_lh_) fmax_lh_ = fmuon_->GetRICHLikeProb(j);
  }
  frich_mass_range_ = fmuon_->GetRICHMass()>0.125&&fmuon_->GetRICHMass()<0.2 ? true : false;
  if (fmax_lh_<=0.12 && frich_mass_range_) frich_pid_ = true;

  fcalo_pid_    = PionPID();

  //Beam background variables
  fsnake_       = IsSnake();
  fbb_cut_      = fsnake_ || fevt_->GetIsSignalInCHANTI() || fevt_->GetIsInteraction() || fkaon_->GetNGTKTracks()>5 || fvtx_.Z() <= 105000 || fvtx_.Z() > 165000;


  //Region definitions
  fpipi_region_ = fmm2_ <= 0.021 && fmm2_ > 0.015;
  fp_region_    = fp_ <= 35 && fp_ > 15;
  fregion_      = GetRegion();

  int flag = fevt_->GetPhotonVetoTimeFlag();
  bool lkr_bits1a = IsBitSet(flag, 2) || IsBitSet(flag, 6) || IsBitSet(flag, 11) || IsBitSet(flag, 9) || IsBitSet(flag, 15); //Option 1a
  bool lkr_bits1b = IsBitSet(flag, 2) || IsBitSet(flag, 6) || IsBitSet(flag, 11) || IsBitSet(flag, 9) || IsBitSet(flag, 14); //Option 1b
  bool lkr_bits1c = IsBitSet(flag, 2) || IsBitSet(flag, 6) || IsBitSet(flag, 11) || IsBitSet(flag, 9) || IsBitSet(flag, 13); //Option 1b
  bool lkr_bits1d = IsBitSet(flag, 2) || IsBitSet(flag, 6) || IsBitSet(flag, 11) || IsBitSet(flag, 9) || IsBitSet(flag, 12); //Option 1b

  //PhotonRejection
  //fphoton_      = fevt_->GetIsSignalInLAV() || fevt_->GetIsSignalInSAC() || fevt_->GetIsSignalInIRC() || fevt_->GetIsSignalInSAV() || lkr_bits1d || fevt_->GetIsAuxiliaryLKr() || fevt_->GetIsLKrInTimeHits();
  fphoton_      = fevt_->GetIsSignalInLAV() || fevt_->GetIsSignalInSAC() || fevt_->GetIsSignalInIRC() || fevt_->GetIsSignalInSAV() || fevt_->GetIsPhotonInLKr() || fevt_->GetIsAuxiliaryLKr()|| fevt_->GetIsLKrInTimeHits();
  fhit_mult_    = fevt_->GetMultAllOldCH7() > 3 ||fevt_->GetMultOldNewCH() > 0 || fevt_->GetMultNewCHLKr() > 0 || fevt_->GetMultOldCHLKr() > 0 || fevt_->GetMultHAC() == 1 || fevt_->GetMultMUV0() == 1 || (fevt_->GetMultLKrMerged() && fevt_->GetMultAllOldCH7() > 0);
  fmult_tot_    = fevt_->GetIsMultiplicity() || fevt_->GetIsSegment() || !fevt_->GetIsOneParticle();

  return;
}
void Kmu2Anal::ResetEvent() {

  fevt_  = NULL;
  fmuon_ = NULL;
  fkaon_ = NULL;

  fvtx_      = TVector3(0.,0.,0.);
  fmm2_      = -99999;
  fmm2_rich_ = -99999;
  fmm2_nomi_ = -99999;
  fp_        = -99999;
  ftrim5_x_  = -99999;
  ftrim5_y_  = -99999;
  ftrim5_r_  = -99999;
  flambda_   = -99999;
  fcda_      = -99999;

  fgtk_match_ = 0.;
  ftgtk_     = -99999;
  fdt_kk_    = -99999;
  fdt_kpi_   = -99999;
  fellipse_  = 0;
  fdt13_     = -99999;
  fdt23_     = -99999;
  fdt12_     = -99999;
  fd13_      = -99999;
  fd23_      = -99999;
  fd12_      = -99999;
  fgtk_cut_  = 0.;

  fcalo_pid_ = 0;
  frich_pid_ = 0;
  fmax_lh_   = -99999;
  frich_mass_range_ = 0;
  fsnake_    = 0;
  fpipi_region_ = 0;
  fp_region_ = 0;
  fbb_cut_   = 0;

  fregion_   = 0;
  fphoton_   = 0;
  fhit_mult_ = 0;
  fmult_tot_ = 0;
  fbox_cut_  = 0;
}

bool Kmu2Anal::EventAnalysis() {

  SetupVariables();
  if (fevt_->GetTriggerMask()==1) return 0;

  // BeamBackgroundCut
  if(fbb_cut_) return 0;
  if(!fgtk_match_) return 0;
  if(fabs(fdt_kk_) > 0.6) return 0;
  if(fabs(fkaon_->GetTGTK()-fmuon_->GetTCHOD()) > 1.1) return 0;
  if(!fmuon_->GetIsRICHSingleRing()) return 0;
  if(fevt_->GetIsBroadMultiVertex()) return 0;
  if(!fp_region_) return 0;

  RandomVetoAnalysis();
  RandomVetoTest();

  KinematicTails();

  return 0;
}


bool Kmu2Anal::RandomVetoTest() {

    //!Photon veto studies criteria
    if(fabs(fevt_->GetMmuMiss())>=0.005 || !fp_region_) return 0;
    if(fmuon_->GetIsMulti()) return 0;
    if(fmuon_->GetIsPositron()) return 0; //E/p < 0.8
    if(fmuon_->GetIsMuon()!= 3) return 0; //Well matched MUV3 candidate
    if(fmuon_->GetCaloMuonProb() <= 0.99) return 0;


    int flag     = fevt_->GetPhotonVetoTimeFlag();
    bool sav_std = IsBitSet(flag, 18) || IsBitSet(flag, 23) || IsBitSet(flag, 27);
    bool sav_5   = IsBitSet(flag, 16) || IsBitSet(flag, 21) || IsBitSet(flag, 25);
    bool sav_6   = IsBitSet(flag, 17) || IsBitSet(flag, 22) || IsBitSet(flag, 26);
    bool lav_25  = IsBitSet(flag, 28);

    bool std_photon_nosav = fevt_->GetIsSignalInLAV() || fevt_->GetIsPhotonInLKr() || fevt_->GetIsAuxiliaryLKr()|| fevt_->GetIsLKrInTimeHits();;
    bool std_photon_nolav = fevt_->GetIsSignalInSAC() || fevt_->GetIsSignalInIRC() || fevt_->GetIsSignalInSAV() || fevt_->GetIsPhotonInLKr() || fevt_->GetIsAuxiliaryLKr()|| fevt_->GetIsLKrInTimeHits();;


    bool photon_sav_std = std_photon_nosav || sav_std;
    bool photon_sav_5   = std_photon_nosav || sav_5;
    bool photon_sav_6   = std_photon_nosav || sav_6;
    bool photon_lav_25  = std_photon_nolav || lav_25;

    // if(fevt_->GetIsSignalInSAC() || fevt_->GetIsSignalInIRC() || fevt_->GetIsSignalInSAV()){
    //cout << "************ ------------START-----------------------***************" << endl;
    //if(fevt_->GetIsSignalInSAV()){
    //    cout << " ------------SAV-----------------------" << endl;
    //    cout << "SAV: 16, 17, 18, 19 = " << IsBitSet(flag, 16) <<  " , " << IsBitSet(flag, 17)  <<  " , " << IsBitSet(flag, 18) <<  " , " << IsBitSet(flag, 19) << endl;
    //    cout << "SAC: 20, 21, 22, 23 = " << IsBitSet(flag, 20) <<  " , " << IsBitSet(flag, 21)  <<  " , " << IsBitSet(flag, 22) <<  " , " << IsBitSet(flag, 23) << endl;
    //    cout << "IRC: 24, 25, 26, 27 = " << IsBitSet(flag, 24) <<  " , " << IsBitSet(flag, 25)  <<  " , " << IsBitSet(flag, 26) <<  " , " << IsBitSet(flag, 27) << endl;
    //    cout << "LAV: 28 , 29        = " << IsBitSet(flag, 28) <<  " , " << IsBitSet(flag, 29)<< endl;
    //}
    //if(fevt_->GetIsSignalInSAC()){
    //    cout << " ------------SAC-----------------------" << endl;
    //    cout << "SAV: 16, 17, 18, 19 = " << IsBitSet(flag, 16) <<  " , " << IsBitSet(flag, 17)  <<  " , " << IsBitSet(flag, 18) <<  " , " << IsBitSet(flag, 19) << endl;
    //    cout << "SAC: 20, 21, 22, 23 = " << IsBitSet(flag, 20) <<  " , " << IsBitSet(flag, 21)  <<  " , " << IsBitSet(flag, 22) <<  " , " << IsBitSet(flag, 23) << endl;
    //    cout << "IRC: 24, 25, 26, 27 = " << IsBitSet(flag, 24) <<  " , " << IsBitSet(flag, 25)  <<  " , " << IsBitSet(flag, 26) <<  " , " << IsBitSet(flag, 27) << endl;
    //    cout << "LAV: 28 , 29        = " << IsBitSet(flag, 28) <<  " , " << IsBitSet(flag, 29)<< endl;
    //}
    //if(fevt_->GetIsSignalInIRC()){
    //    cout << " ------------IRC-----------------------" << endl;
    //    cout << "SAV: 16, 17, 18, 19 = " << IsBitSet(flag, 16) <<  " , " << IsBitSet(flag, 17)  <<  " , " << IsBitSet(flag, 18) <<  " , " << IsBitSet(flag, 19) << endl;
    //    cout << "SAC: 20, 21, 22, 23 = " << IsBitSet(flag, 20) <<  " , " << IsBitSet(flag, 21)  <<  " , " << IsBitSet(flag, 22) <<  " , " << IsBitSet(flag, 23) << endl;
    //    cout << "IRC: 24, 25, 26, 27 = " << IsBitSet(flag, 24) <<  " , " << IsBitSet(flag, 25)  <<  " , " << IsBitSet(flag, 26) <<  " , " << IsBitSet(flag, 27) << endl;
    //    cout << "LAV: 28 , 29        = " << IsBitSet(flag, 28) <<  " , " << IsBitSet(flag, 29)<< endl;
    //}
    fHisto->FillHisto("KMU2_TEST_lambda_vs_p_0", fp_, flambda_);

    if(!fphoton_)
        fHisto->FillHisto("KMU2_TEST_lambda_vs_p_photon_std", fp_, flambda_);
    if(!photon_sav_std)
        fHisto->FillHisto("KMU2_TEST_lambda_vs_p_photon_std_test", fp_, flambda_);
    if(!photon_sav_5)
        fHisto->FillHisto("KMU2_TEST_lambda_vs_p_photon_sav5", fp_, flambda_);
    if(!photon_sav_6)
        fHisto->FillHisto("KMU2_TEST_lambda_vs_p_photon_sav6", fp_, flambda_);
    if(!photon_lav_25)
        fHisto->FillHisto("KMU2_TEST_lambda_vs_p_photon_lav25", fp_, flambda_);

    return 1;
}
bool Kmu2Anal::RandomVetoAnalysis() {

  //!Photon veto studies criteria
  if(fabs(fevt_->GetMmuMiss())>=0.005 || !fp_region_) return 0;
  if(fmuon_->GetIsMulti()) return 0;
  if(fmuon_->GetIsPositron()) return 0; //E/p < 0.8
  if(fmuon_->GetIsMuon()!= 3) return 0; //Well matched MUV3 candidate
  if(fmuon_->GetCaloMuonProb() <= 0.99) return 0;

  int flag = fevt_->GetPhotonVetoTimeFlag();
  bool lkr_bits1a = IsBitSet(flag, 2) || IsBitSet(flag, 6) || IsBitSet(flag, 11) || IsBitSet(flag, 9) || IsBitSet(flag, 15); //Option 1a
  bool lkr_bits1b = IsBitSet(flag, 2) || IsBitSet(flag, 6) || IsBitSet(flag, 11) || IsBitSet(flag, 9) || IsBitSet(flag, 14); //Option 1b
  bool lkr_bits1c = IsBitSet(flag, 2) || IsBitSet(flag, 6) || IsBitSet(flag, 11) || IsBitSet(flag, 9) || IsBitSet(flag, 13); //Option 1b
  bool lkr_bits1d = IsBitSet(flag, 2) || IsBitSet(flag, 6) || IsBitSet(flag, 11) || IsBitSet(flag, 9) || IsBitSet(flag, 12); //Option 1b
  //cout << flag << "  " << IsBitSet(flag, 10) << "  "<< x << endl;

  fHisto->FillHisto("KMU2_mmiss_vs_p", fp_, flambda_);
  fHisto->FillHisto("KMU2_lambda_vs_p", fp_, flambda_);
  if(!fevt_->GetIsSignalInLAV())
    fHisto->FillHisto("KMU2_lambda_vs_p_LAV", fp_, flambda_);
  if(!fevt_->GetIsAuxiliaryLKr() && !fevt_->GetIsPhotonInLKr())
    fHisto->FillHisto("KMU2_lambda_vs_p_LKr", fp_, flambda_);
  if(!fevt_->GetIsAuxiliaryLKr() && !fevt_->GetIsPhotonInLKr() && !fevt_->GetIsLKrInTimeHits())
    fHisto->FillHisto("KMU2_lambda_vs_p_LKr_EH", fp_, flambda_);
  //cout << fevt_->GetIsLKrInTimeHits() << endl;

  if(!fevt_->GetIsAuxiliaryLKr() && !lkr_bits1a)
    fHisto->FillHisto("KMU2_lambda_vs_p_LKr_1A", fp_, flambda_);
  if(!fevt_->GetIsAuxiliaryLKr() && !lkr_bits1a && !fevt_->GetIsLKrInTimeHits())
      fHisto->FillHisto("KMU2_lambda_vs_p_LKr_1A_EH", fp_, flambda_);

  if(!fevt_->GetIsAuxiliaryLKr() && !lkr_bits1b)
      fHisto->FillHisto("KMU2_lambda_vs_p_LKr_1B", fp_, flambda_);
  if(!fevt_->GetIsAuxiliaryLKr() && !lkr_bits1b && !fevt_->GetIsLKrInTimeHits())
      fHisto->FillHisto("KMU2_lambda_vs_p_LKr_1B_EH", fp_, flambda_);

  if(!fevt_->GetIsAuxiliaryLKr() && !lkr_bits1c)
      fHisto->FillHisto("KMU2_lambda_vs_p_LKr_1C", fp_, flambda_);
  if(!fevt_->GetIsAuxiliaryLKr() && !lkr_bits1c && !fevt_->GetIsLKrInTimeHits())
      fHisto->FillHisto("KMU2_lambda_vs_p_LKr_1C_EH", fp_, flambda_);

  if(!fevt_->GetIsAuxiliaryLKr() && !lkr_bits1d)
      fHisto->FillHisto("KMU2_lambda_vs_p_LKr_1D", fp_, flambda_);
  if(!fevt_->GetIsAuxiliaryLKr() && !lkr_bits1d && !fevt_->GetIsLKrInTimeHits())
      fHisto->FillHisto("KMU2_lambda_vs_p_LKr_1D_EH", fp_, flambda_);

  if(!fevt_->GetIsSignalInSAV() && !fevt_->GetIsSignalInIRC() && !fevt_->GetIsSignalInSAC())
    fHisto->FillHisto("KMU2_lambda_vs_p_SAV", fp_, flambda_);

  //!Cluster-based photon rejection
  if(!fphoton_){
    fHisto->FillHisto("KMU2_lambda_vs_p_Photon", fp_, flambda_);

    //!LKr, CHOD, NewCHOD multiplicity
    if (!fhit_mult_) {

      fHisto->FillHisto("KMU2_lambda_vs_p_Photon_Mult", fp_, flambda_);

      //!OneParticle (STRAW mult)
      if(fevt_->GetIsOneParticle()){
        fHisto->FillHisto("KMU2_lambda_vs_p_Photon_Mult_OnePart", fp_, flambda_);

        //!Full RV
        if(!fevt_->GetIsSegment())
          fHisto->FillHisto("KMU2_lambda_vs_p_Photon_Mult_OnePart_Segments", fp_, flambda_);
      }

    }
  }

  return true;
}

bool Kmu2Anal::IsBitSet(int n, int bit) {

    return 1 == ( (n >> bit) & 1);
}
bool Kmu2Anal::KinematicTails() {

  if(fcalo_pid_) return 0;
  if(fhit_mult_) return 0;
  if(fevt_->GetIsSegment()) return 0;
  if(!fevt_->GetIsOneParticle()) return 0;
  if(fevt_->GetIsBroadMultiVertex()) return 0;

  if(fmuon_->GetCaloMuonProb() <= 0.99) return 0;
  //if(fmuon_->GetCaloMuonProb() <= 0.95) return 0;
  //if(fmuon_->GetIsMuon()!= 3) return 0; //Well matched MUV3 candidate
  if(fphoton_) return 0;
  if(fp_ <= 15) return 0;


  int bin = -1;
  if(fp_ <= 15 || fp_ > 50) return 0;
  if(fp_ <= 20 && fp_ > 15) bin = 1;
  if(fp_ <= 25 && fp_ > 20) bin = 2;
  if(fp_ <= 30 && fp_ > 25) bin = 3;
  if(fp_ <= 35 && fp_ > 30) bin = 4;
  if(fp_ <= 40 && fp_ > 35) bin = 5;
  if(fp_ <= 45 && fp_ > 40) bin = 6;
  if(fp_ <= 50 && fp_ > 45) bin = 7;

  fHisto->FillHisto("KINE_lambda", flambda_);

  //if((int)fevt_->GetTimestamp()% 256 < 35 && (int)fevt_->GetTimestamp()%256 > 28) return 0;
  if(fkaon_->GetGTKExtraHits()) return 0;

  fHisto->FillHisto("KINE_mmiss_vs_p", fp_, fmm2_);
  fHisto->FillHisto("KINE_mmiss_vs_mmissRICH", fmm2_, fmm2_rich_);
  fHisto->FillHisto("KINE_mmiss_vs_mmissNominalK", fmm2_, fmm2_nomi_);

  if(frich_pid_){

      fHisto->FillHisto("KINERICH_mmiss_vs_p", fp_, fmm2_);
      fHisto->FillHisto("KINERICH_mmiss_vs_mmissRICH", fmm2_, fmm2_rich_);
      fHisto->FillHisto("KINERICH_mmiss_vs_mmissNominalK", fmm2_, fmm2_nomi_);
  }

  bool regions = fregion_ <= 2 || fregion_ == 30 || fregion_ == 200;
  if(!regions) return 0;
  if(fregion_ == 0) return 0;

  fHisto->FillHisto(Form("KINE_r%d_mmiss_vs_p", fregion_), fp_, fmm2_);
  fHisto->FillHisto(Form("KINE_r%d_lambda_vs_p",fregion_), fp_, flambda_);
  fHisto->FillHisto(Form("KINE_r%d_mmiss_vs_mmissRICH", fregion_), fmm2_, fmm2_rich_);
  fHisto->FillHisto(Form("KINE_r%d_mmiss_vs_mmissNominalK", fregion_), fmm2_, fmm2_nomi_);

  fHisto->FillHisto(Form("KINE_r%d_mmiss_vs_mmissRICH_bin%d", fregion_, bin), fmm2_, fmm2_rich_);
  fHisto->FillHisto(Form("KINE_r%d_mmiss_vs_mmissNominalK_bin%d", fregion_, bin), fmm2_, fmm2_nomi_);

  if(!frich_pid_) return 0;

  // //Putting the proper 3D m2miss regions
  // bool r1 = false;
  // bool r2 = false;

  // if (fp_<=20.)
  //     r1 = (fmm2_>0.    && fmm2_<=0.01)  && (fmm2_nomi_>-0.005&&fmm2_nomi_<=0.0135) && (fmm2_rich_>0.    &&fmm2_rich_<=0.01);
  // if (fp_>20. && fp_<=25.)
  //     r1 = (fmm2_>0.    && fmm2_<=0.01)  && (fmm2_nomi_>-0.005&&fmm2_nomi_<=0.0135) && (fmm2_rich_>0.    &&fmm2_rich_<=0.02);
  // if (fp_>25.)
  //     r1 = (fmm2_>0.    && fmm2_<=0.01)  && (fmm2_nomi_>0.    &&fmm2_nomi_<=0.0135) && (fmm2_rich_>-0.005&&fmm2_rich_<=0.02);

  // r2 = (fmm2_>0.026 && fmm2_<=0.068) && (fmm2_nomi_>0.024 &&fmm2_nomi_<=0.068)  && (fmm2_rich_>0.02  &&fmm2_rich_<=0.07);

  // if(r1) fregion_ = 1;
  // if(r2) fregion_ = 2;


  fHisto->FillHisto(Form("KINERICH_r%d_mmiss_vs_p", fregion_), fp_, fmm2_);
  fHisto->FillHisto(Form("KINERICH_r%d_lambda_vs_p",fregion_), fp_, flambda_);
  fHisto->FillHisto(Form("KINERICH_r%d_mmiss_vs_mmissRICH", fregion_), fmm2_, fmm2_rich_);
  fHisto->FillHisto(Form("KINERICH_r%d_mmiss_vs_mmissNominalK", fregion_), fmm2_, fmm2_nomi_);

  fHisto->FillHisto(Form("KINERICH_r%d_mmiss_vs_mmissRICH_bin%d", fregion_, bin), fmm2_, fmm2_rich_);
  fHisto->FillHisto(Form("KINERICH_r%d_mmiss_vs_mmissNominalK_bin%d", fregion_, bin), fmm2_, fmm2_nomi_);

  return true;
}

void Kmu2Anal::DiscriminantNormalization() {

  double pass_dt = 0.001; // mm

  fPDFKaonDT_.clear();
  fIntPDFKaonDT_ = 0;
  for (int jbindt(0); jbindt<3000; jbindt++) { // +-3 ns
    double dt = pass_dt*jbindt+0.5*pass_dt;
    fIntPDFKaonDT_ += (PDFKaonDT(dt)+PDFKaonDT(-dt))*pass_dt/0.01; // compute the total integral of the PDF
    fPDFKaonDT_.push_back(fIntPDFKaonDT_); // fill the cumulative distribution function
  }
}

Double_t Kmu2Anal::Discriminant(const Double_t &dt_val) {
  double D = 0;

  if(fabs(dt_val) >= 3) return D;

  double pkdt    = 0;
  double pass_dt = 0.001;
  for (auto ikdt = fPDFKaonDT_.begin(); ikdt != fPDFKaonDT_.end();++ikdt) {
    double id = (double)(distance(fPDFKaonDT_.begin(),ikdt))*pass_dt; // compute the time corresponding to each element of the cumulative distribution function
    if(fabs(dt_val) < id) {
      pkdt = id/fIntPDFKaonDT_;

      break;
    }
  }

  D = (1 - pkdt);
  return D;
}
Double_t Kmu2Anal::PDFKaonDT(Double_t dt) {
  Double_t dtpar[3];

  dtpar[0] = 0.02;
  dtpar[1] = 0.;
  dtpar[2] = 0.2;

  Double_t dtg1 = dtpar[0]*exp(-0.5*((dt-dtpar[1])/dtpar[2])*((dt-dtpar[1])/dtpar[2]));
  return dtg1;
}

int Kmu2Anal::PionPID() {

  bool D = 0;

  if (fmuon_->GetIsMUV3()) D = 1;
  if (fmuon_->GetIsMulti()) D = 1;
  if (fmuon_->GetIsMip()) D = 1;

  Double_t caloEnergy = fmuon_->GetCaloEnergy();
  Double_t r1 = fmuon_->GetMUV1Energy()/caloEnergy;
  Double_t r2 = fmuon_->GetMUV2Energy()/caloEnergy;
  Double_t elkr = fmuon_->GetLKrEnergy();
  Double_t seedratio = fmuon_->GetLKrSeedEnergy()/elkr;
  Double_t cellratio = fmuon_->GetLKrNCells()/elkr;
  bool isLKrEM = 0;
  auto vcut       = TMath::Max(0.7,0.98-0.4596*exp(-(fp_-11.44)/5.27));
  bool IsCaloPion = fmuon_->GetCaloPionProb()>=vcut;

  if (r1<0.01 && r2<0.01) {
    if (seedratio>0.2&&cellratio<=3) isLKrEM = 1;
    if (seedratio<=0.2&&cellratio<1.8) isLKrEM = 1;
    if (seedratio>0.35) isLKrEM = 1;
    if (seedratio<0.05) isLKrEM = 1;
  }
  if (seedratio>0. && seedratio<0.8 && cellratio<1.4) isLKrEM = 1;

  if(!IsCaloPion) D = 1;
  if (isLKrEM) D = 1;
  if (caloEnergy>1.2*fp_) D = 1;
  if (elkr>0.8*fp_) D = 1;
  if (!fmuon_->GetIsGoodMUV1() && fmuon_->GetIsGoodMUV2()) D = 1;

  return D > 0 ? 0 : 1;
}

double Kmu2Anal::Kmu2KinematicBound(){

  double mpi = 0.13957018;
  double mmu = 0.1056583745;

  return (pow(mpi,2) - pow(mmu,2))*(1 - 75/fp_);
}

bool Kmu2Anal::IsSnake() {
  PnnNA62Event *evt = GetEvent();
  PnnNA62DownstreamParticle *pion = (PnnNA62DownstreamParticle *)evt->GetDownstreamParticle();
  double zvtx = evt->GetVertex().Z();
  // double rst1 = sqrt((pion->GetPosStraw1().X()-101.2)*(pion->GetPosStraw1().X()-101.2)+pion->GetPosStraw1().Y()*pion->GetPosStraw1().Y());
  double rst1 = sqrt((pion->GetPosStraw1X()-101.2)*(pion->GetPosStraw1X()-101.2)+pion->GetPosStraw1Y()*pion->GetPosStraw1Y());
  double b1 = -0.004;
  double a1 = 315-b1*105000.;
  double r115 = a1+b1*115000.;
  double b2 = -(900-r115)/10000.;
  double a2 = 900-b2*105000.;
  double b3 = -0.00983333333;
  double a3 = 780-b3*105000.;
  double cut1 = a1+b1*zvtx;
  double cut2 = a2+b2*zvtx;
  double cut3 = a3+b3*zvtx;
  bool isSignal = rst1>cut1 && rst1>cut2 && rst1<cut3 && (zvtx)<165000.;
  return !isSignal;
}
int Kmu2Anal::GetRegion() {

    bool isPNNRegion1 = 0;
    double bound = Kmu2KinematicBound();

    // if(fp_ <= 20)
    //     isPNNRegion1 = (fmm2_ > 0 && fmm2_ <= 0.01) && (fmm2_nomi_ > -0.005 && fmm2_nomi_ <= 0.0135) && (fmm2_rich_ > 0. && fmm2_rich_ <= 0.01);
    // if(fp_ > 20 && fp_ <= 25)
    //     isPNNRegion1 = (fmm2_ > 0 && fmm2_ <= 0.01) && (fmm2_nomi_ > -0.005 && fmm2_nomi_ <= 0.0135) && (fmm2_rich_ > 0. && fmm2_rich_ <= 0.02);
    // if(fp_ > 25)
    //     isPNNRegion1 = (fmm2_ > 0 && fmm2_ <= 0.01) && (fmm2_nomi_ > 0 && fmm2_nomi_ <= 0.0135)      && (fmm2_rich_ > -0.005 && fmm2_rich_ <= 0.02);

    if(fp_ <= 20)
        isPNNRegion1 = (fmm2_ > 0 && fmm2_ <= 0.01) && (fmm2_nomi_ > -0.005 && fmm2_nomi_ <= 0.0135);
    if(fp_ > 20 && fp_ <= 25)
        isPNNRegion1 = (fmm2_ > 0 && fmm2_ <= 0.01) && (fmm2_nomi_ > -0.005 && fmm2_nomi_ <= 0.0135);
    if(fp_ > 25)
        isPNNRegion1 = (fmm2_ > 0 && fmm2_ <= 0.01) && (fmm2_nomi_ > 0 && fmm2_nomi_ <= 0.0135)     ;

    bool isPNNRegion2 = fmm2_ <= 0.068 && fmm2_ > 0.026 && (fmm2_nomi_ > 0.024 && fmm2_nomi_ <= 0.068);
    bool isCR1        = fmm2_ > 0.01  && fmm2_<=0.015;
    bool isCR2        = fmm2_>0.021 && fmm2_ <= 0.026;
    bool isCRmu       = fmm2_<=0     && fmm2_>bound+0.0036;
    bool isCR3pi      = fmm2_>0.068 && fmm2_<=0.072;
    bool isCRUp       = (fmm2_>bound-0.0070 && fmm2_<=bound-0.0060);
    bool isK2piRegion = (fmm2_>0.015 && fmm2_<=0.021);
    bool isKmu2Region = (fmm2_>-0.05 && fmm2_<=bound+0.0036) /*(fmm2_>bound-0.0060 && fmm2_<= bound+0.0036)*/;
    bool isK3PiRegion  = (fmm2_>0.072);
    bool isUpRegion   = fmm2_<=bound-0.0070;

    if (isPNNRegion1) return 1;
    if (isPNNRegion2) return 2;
    if (isCR1) return 10;
    if (isCR2) return 20;
    if (isCRmu) return 30;
    if (isCR3pi) return 40;
    if (isCRUp) return 50;
    if (isK2piRegion) return 100;
    if (isKmu2Region) return 200;
    if (isK3PiRegion) return 300;
    if (isUpRegion) return 400;

    return 0;
}
