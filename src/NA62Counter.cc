#include "NA62Counter.hh"

NA62Counter::NA62Counter() { }

NA62Counter::~NA62Counter() { }

void NA62Counter::Set(string name) {
  fCounters.insert(pair<string,int>(name,0));  
}

void NA62Counter::Add(string name) {
  auto iter=fCounters.find(name); 
  if (iter!=fCounters.end()) iter->second+=1;
}

int NA62Counter::Get(string name) {
  auto iter=fCounters.find(name); 
  if (iter!=fCounters.end()) return iter->second;
  return 0;
}
