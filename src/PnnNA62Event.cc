#include "PnnNA62Event.hh"

ClassImp(PnnNA62Event)
ClassImp(PnnNA62DownstreamParticle)
ClassImp(PnnNA62UpstreamParticle)
ClassImp(PnnNA62Trigger)

PnnNA62Event::PnnNA62Event() {
    fDownstreamParticle = new PnnNA62DownstreamParticle();
    fUpstreamParticle   = new PnnNA62UpstreamParticle();
    fTrigger            = new PnnNA62Trigger();
    Clear();
}

PnnNA62Event::~PnnNA62Event()
{
    delete fDownstreamParticle;
    delete fUpstreamParticle;
    delete fTrigger;
}

void PnnNA62Event::Clear(Option_t *opt) {
    TObject::Clear(opt);
    fBurstNumber = -1;
    fRunNumber = -1;
    fEventNumber = -1;
    fTimestamp = -1;
    fTriggerMask = -1;
    fIsK2piEvent = 0;
    fLambda = -1.;
    fLambdaFW = -1.;
    fVertex.SetXYZ(-99999.,-99999.,-99999.);
    fCDA = -1.;
    fMMiss = -100.;
    fMMissRich = -100.;
    fMMissNominalK = -100.;
    fMmuMiss = -100.;
    fIsHighToT = 0;
    fIsInteraction = 0;
    fIsEarlyDecay = 0;
    fIsKTAGBeamDiscriminant = -1.;
    fIsRICHBeamDiscriminant = -1.;
    fIsLKrInTimeHits = 0.;
    fLKrInTimeEnergy = 0.;
    fSACToT = -999999;
    fSACToT_TH_LH = 0;
    fSACToT_TL_TH = 0;
    fIRCToT = -999999;
    fIRCToT_TH_LH = 0;
    fIRCToT_TL_TH = 0;
    fSACEnergyADC = -999999;
    fIRCEnergyADC = -999999;
    fSACTimeTDC = -999999;
    fIRCTimeTDC = -999999;
    fSACTimeADC = -999999;
    fIRCTimeADC = -999999;

    fIsPhotonInLKr = 0;
    fIsSignalInLAV = 0;
    fIs1PhotonInLAV = 0;
    fIs2PhotonsInLAV = 0;
    fIsSignalInIRC = 0;
    fIsSignalInSAC = 0;
    fIsSignalInSAV = 0;
    fIsAuxiliaryLKr = 0;
    fIsSegment = 0;
    fIsMultiplicity = 0;
    fPhotonVetoTimeFlag = 0;
    fMultAllOldCH7 = 0;
    fMultOldCHLKr = 0;
    fMultOldNewCH = 0;
    fMultNewCHLKr = 0;
    fMultLKrMerged = 0;
    fMultMUV0 = 0;
    fMultHAC = 0;
    fIsOneParticle = 0;
    fIsBroadMultiVertex = 0;
    fDownstreamParticle->Clear();
    fUpstreamParticle->Clear();
    fTrigger->Clear();
}


//////////////////////////////////////
// Class PnnNA62DownstreamParticle //////
//////////////////////////////////////

PnnNA62DownstreamParticle::PnnNA62DownstreamParticle() {
    Clear();
}

PnnNA62DownstreamParticle::~PnnNA62DownstreamParticle()
{ }

void PnnNA62DownstreamParticle::Clear(Option_t *opt) {
    TObject::Clear(opt);
    fP.SetXYZ(0,0,0);
    fSlopeXAfterMagnet=-99999.;
    fSlopeYAfterMagnet=-99999.;
    fPositionXAfterMagnet=-99999.;
    fPositionYAfterMagnet=-99999.;
    fPositionZAfterMagnet=-99999.;
    fTSpectrometer = -99999.;
    fTRICH = -99999.;
    fTCHOD = -99999.;
    fTNewCHOD = -99999.;
    fTLKr = -99999.;
    fIsMuon = 0;
    fIsMip = 0;
    fIsMulti = 0;
    fIsPositron = 0;
    fIsGoodMUV1 = 0;
    fIsGoodMUV2 = 0;
    fIsMUV3 = 0;
    fCaloMuonProb = -99999.;
    fCaloPionProb = -99999.;
    fCaloEnergy = 0.;
    fMUV1Energy = 0.;
    fMUV2Energy = 0.;
    fLKrEnergy = 0.;
    fLKrSeedEnergy = 0.;
    fLKrNCells = 0.;
    fDownstreamDistance = 999999999;
    fIsRICHLike = 0;
    fIsRICHSingleRing = 0;
    fRICHSingleNHits = 0;
    fRICHLikeMost = -1;
    for (int j=0; j<5; j++) fRICHLikeProb[j] = -99999.;
    fRICHRadius = -99999.;
    fTrackCentredTRICH = -999999;
    fTrackCentredRICHMass = -999999;
    fTrackCentredRICHRadius = -999999;
    fRICHMass = -99999.;
    fRICHMomentum = 0;
    fPosTrim5X = -99999.;
    fPosTrim5Y = -99999.;
    fPosGTK3X = -99999.;
    fPosGTK3Y = -99999.;
    fPosStraw1X = -99999.;
    fPosStraw1Y = -99999.;
}

//////////////////////////////////////
// Class PnnNA62UpstreamParticle //////
//////////////////////////////////////

PnnNA62UpstreamParticle::PnnNA62UpstreamParticle() {
    Clear();
}

PnnNA62UpstreamParticle::~PnnNA62UpstreamParticle()
{ }

void PnnNA62UpstreamParticle::Clear(Option_t *opt) {
    TObject::Clear(opt);
    fP.SetXYZ(-99999.,-99999.,-99999.);
    fTKTAG = -99999.;
    fTGTK = -99999.;
    fTGTK1 = -99999.;
    fTGTK2 = -99999.;
    fTGTK3 = -99999.;
    fTCHANTI = -99999.;
    fPosGTK1X= -99999.;
    fPosGTK1Y= -99999.;
    fPosGTK2X= -99999.;
    fPosGTK2Y= -99999.;
    fPosGTK3X= -99999.;
    fPosGTK3Y= -99999.;
    fNGTKTracks = 0;
    fNGTKInTimeTracks = 0;

    fGTKExtraHits = 0;
}

///////////////////////////
// Class PnnNA62Trigger //////
///////////////////////////

PnnNA62Trigger::PnnNA62Trigger() {
    Clear();
}

PnnNA62Trigger::~PnnNA62Trigger()
{ }

void PnnNA62Trigger::Clear(Option_t* opt) {
    TObject::Clear(opt);
    fIsTrigger = -1;
    fNewCHODL0 = -1;
    fQxL0      = -1;
    fUTMCL0    = -1;
    fMUV3L0    = -1;
    fLAVL0     = -1;
    fLKrL0     = -1;
    fNewCHODRef= -1;
    fRICHRef   = -1;
    fRICHL0    = -1;
}
