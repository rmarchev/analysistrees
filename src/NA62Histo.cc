#include "NA62Histo.hh"

NA62Histo::NA62Histo()
{
}

void NA62Histo::Book(TString file)
{
  ifstream histoFile(file.Data());
   if(!histoFile)
//  if(histoFile)
  {
    perror("Histograms File :");
    exit(1);
  }

  TString Line;
  while(Line.ReadLine(histoFile))
  {
    if(Line.BeginsWith("#")) continue;
    TObjArray *l = Line.Tokenize(" ");
    if (l->GetEntriesFast()<5)
    {
      TString sName = ((TObjString*)(l->At(0)))->GetString();
      Int_t nBins    = ((TObjString*)(l->At(1)))->GetString().Atoi();
      Double_t fMin  = ((TObjString*)(l->At(2)))->GetString().Atof();
      Double_t fMax  = ((TObjString*)(l->At(3)))->GetString().Atof();
      fHistos.Add(new TH1F(sName," ",nBins,fMin,fMax));
    } else {
      TString sName = ((TObjString*)(l->At(0)))->GetString();
      Int_t nBins1    = ((TObjString*)(l->At(1)))->GetString().Atoi();
      Double_t fMin1  = ((TObjString*)(l->At(2)))->GetString().Atof();
      Double_t fMax1  = ((TObjString*)(l->At(3)))->GetString().Atof();
      Int_t nBins2    = ((TObjString*)(l->At(4)))->GetString().Atoi();
      Double_t fMin2  = ((TObjString*)(l->At(5)))->GetString().Atof();
      Double_t fMax2  = ((TObjString*)(l->At(6)))->GetString().Atof();
      fHistos.Add(new TH2F(sName," ",nBins1,fMin1,fMax1,nBins2,fMin2,fMax2));
    }
  }
  histoFile.close();
}

void NA62Histo::Write()
{
  TIterator *hst=fHistos.MakeIterator();
  TH1 *h1;
  while((h1=(TH1*)hst->Next()))h1->Write();
}
