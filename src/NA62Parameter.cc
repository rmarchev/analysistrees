#include "NA62Parameter.hh"

NA62Parameter::NA62Parameter() { }

NA62Parameter::~NA62Parameter() { }

void NA62Parameter::Set(string name, double val) {
  fParameters.insert(pair<string,double>(name,val));  
}

double NA62Parameter::Get(string name) {
  auto iter=fParameters.find(name); 
  if (iter!=fParameters.end()) return iter->second;
  return 0;
}
