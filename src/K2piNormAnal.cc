#include "K2piNormAnal.hh"

K2piNormAnal::K2piNormAnal() {
  DiscriminantNormalization();
}

K2piNormAnal::~K2piNormAnal() {
}

void K2piNormAnal::SetParameter() {

}

//void K2piNormAnal::InitHisto() {
//}
void K2piNormAnal::InitCounter() {

  fFlagSelectionForBDT = true; // false to use cut-based selection
  fFlagReaderBDT = true;

  if(fFlagReaderBDT){
    const char* inputVars[] = {"Xtrim5","Ytrim5","Xstraw1","Ystraw1","Rstraw1","Xvtx","Yvtx","Zvtx","ThetaX","ThetaY"};
    fTMVAReader = new TMVA::Reader("Verbose",0);
    for (int i=0; i<10; i++) fTMVAReader->AddVariable(TString(inputVars[i]),&fTMVAVariables[i]);
    fTMVAReader->BookMVA("myBDT","/afs/cern.ch/work/f/fbriziol/public/BDTweights.xml");
  }

}

void K2piNormAnal::SetupVariables() {

  ResetEvent();

  fevt_  = GetEvent();
  fpion_ = (PnnNA62DownstreamParticle *)fevt_->GetDownstreamParticle();
  fkaon_ = (PnnNA62UpstreamParticle *)fevt_->GetUpstreamParticle();
  ftrig_ = (PnnNA62Trigger *)fevt_->GetTrigger();

  fRunNumber = fevt_->GetRunNumber();
  fIsMC = false;
  if(fRunNumber<8500) fIsMC = true; // it works only with MC generated with 2017 runs as reference!

  //Kinematic variables + instantaneous intensity
  fvtx_      = fevt_->GetVertex();
  fmm2_      = fevt_->GetMMiss();
  fmm2_rich_ = fevt_->GetMMissRich();
  fmm2_nomi_ = fevt_->GetMMissNominalK();
  fp_        = fpion_->GetP().Mag();
  ftrim5_x_  = fpion_->GetPosTrim5X();
  ftrim5_y_  = fpion_->GetPosTrim5Y();
  ftrim5_r_  = sqrt(pow(ftrim5_x_,2) + pow(ftrim5_y_, 2));
  fstraw1_x_ = fpion_->GetPosStraw1X();
  fstraw1_y_ = fpion_->GetPosStraw1Y();
  fstraw1_r_ = sqrt( pow(fstraw1_x_ - 101.2, 2) + pow(fstraw1_y_, 2));
  flambda_   = fevt_->GetLambda();
  fcda_      = fevt_->GetCDA();
  fbox_cut_  = fabs(ftrim5_x_)<100. && fabs(ftrim5_y_)<100.;
  fthx_      = fpion_->GetP().X()/fpion_->GetP().Z();
  fthy_      = fpion_->GetP().Y()/fpion_->GetP().Z();

  // fTMVACutValue = 0.577484; // old
  fTMVACutValue = 0.631603 ;
  fTMVAUpstreamCut = true;
  // BDT reader            
  if(fFlagReaderBDT){
    fTMVAVariables[0] = ftrim5_x_ ;
    fTMVAVariables[1] = ftrim5_y_ ;
    fTMVAVariables[2] = fstraw1_x_ ;
    fTMVAVariables[3] = fstraw1_y_ ;
    fTMVAVariables[4] = fstraw1_r_ ;
    fTMVAVariables[5] = fvtx_.X() ;
    fTMVAVariables[6] = fvtx_.Y() ;
    fTMVAVariables[7] = fvtx_.Z() ;
    fTMVAVariables[8] = fthx_ ;
    fTMVAVariables[9] = fthy_ ;
    fTMVAValue = fTMVAReader->EvaluateMVA("myBDT");
    if(fTMVAValue>fTMVACutValue) fTMVAUpstreamCut = false; // don't cut
  }

  //GTK timing variables and related cuts
  if ((fevt_->GetIsKTAGBeamDiscriminant()>0.03 || fevt_->GetIsRICHBeamDiscriminant()>0.03) && fevt_->GetIsKTAGBeamDiscriminant() > 0.005 && fevt_->GetIsRICHBeamDiscriminant() > 0.005)
    fgtk_match_ = true;

  fdt_kk_    = fkaon_->GetTGTK()-fkaon_->GetTKTAG();
  fdt_kpi_   = fkaon_->GetTKTAG() - fpion_->GetTRICH();

  fIsInTime = true ;
  if(fabs(fdt_kk_)>0.5 || fabs(fdt_kpi_)>0.5) fIsInTime = false;

  //RICH variables
  for (int j=0; j<5; j++) {
    if (j==3) continue;
    if (fpion_->GetRICHLikeProb(j)> fmax_lh_) fmax_lh_ = fpion_->GetRICHLikeProb(j);
  }
  frich_mass_range_ = fpion_->GetRICHMass()>0.125&&fpion_->GetRICHMass()<0.2 ? true : false;
  if (fmax_lh_<=0.12 && frich_mass_range_) frich_pid_ = true;

  fcalo_pid_    = PionPID();

  //Beam background variables
  fsnake_       = IsSnake();
  // fbb_cut_      = fsnake_ || fevt_->GetIsSignalInCHANTI() || fevt_->GetIsInteraction()  || fkaon_->GetNGTKTracks()>5 || fvtx_.Z() <= 105000 || fvtx_.Z() > 165000;
  // fbb_cut_      = fevt_->GetIsHighToT() || fevt_->GetIsSignalInCHANTI() || fevt_->GetIsInteraction()  || fkaon_->GetNGTKTracks()>5;
  fbb_cut_      = fsnake_ || fevt_->GetIsSignalInCHANTI() || fevt_->GetIsInteraction() || fkaon_->GetNGTKTracks()>5 || fvtx_.Z() <= 105000 || fvtx_.Z() > 165000;
  fbb_cut_BDTTrees = fevt_->GetIsSignalInCHANTI() || fevt_->GetIsInteraction() || fkaon_->GetNGTKTracks()>5 || fvtx_.Z() <= 105000 || fvtx_.Z() > 175000 ;

  //Region definitions
  fpipi_region_ = fmm2_ <= 0.021 && fmm2_ > 0.015;
  fp_region_    = fp_ <= 40 && fp_ > 15;

  //PhotonRejection
  fphoton_      = fevt_->GetIsSignalInLAV() || fevt_->GetIsSignalInSAC() || fevt_->GetIsSignalInIRC() || fevt_->GetIsSignalInSAV() || fevt_->GetIsPhotonInLKr() || fevt_->GetIsAuxiliaryLKr()|| fevt_->GetIsLKrInTimeHits();
  fhit_mult_    = fevt_->GetMultAllOldCH7() > 3 ||fevt_->GetMultOldNewCH() > 0 || fevt_->GetMultNewCHLKr() > 0 || fevt_->GetMultOldCHLKr() > 0 || fevt_->GetMultHAC() == 1 || fevt_->GetMultMUV0() == 1 || (fevt_->GetMultLKrMerged() && fevt_->GetMultAllOldCH7() >0);
  fmult_tot_    = fevt_->GetIsMultiplicity() || fevt_->GetIsSegment() || !fevt_->GetIsOneParticle() || fevt_->GetIsBroadMultiVertex();
  fmult_tot_BDTTrees = fhit_mult_ || fevt_->GetIsSegment() || !fevt_->GetIsOneParticle() || fevt_->GetIsBroadMultiVertex();

  fregion_      = GetRegion();
  fIsNormRegion = false;
  if(fregion_==100 && fp_>15 && fp_<35) fIsNormRegion = true;

  fBlindCut = false;
  if(!fIsMC && fregion_!=100 && fregion_!=200 && fregion_!=300 && fregion_!=400) fBlindCut = true;

  return;
}

bool K2piNormAnal::EventAnalysis() {

  SetupVariables(); // Important!!! : ResetEvent() must always be called before the Setup

  if(fBlindCut) return 0; // blinding signal and control regions

  fHisto->FillHisto("K2PINORM_mmiss_vs_p_0",fp_,fmm2_);
  fHisto->FillHisto("K2PINORM_mmiss_vs_mmissRICH_0",fmm2_,fmm2_rich_);
  fHisto->FillHisto("K2PINORM_mmiss_vs_mmissNominalK_0",fmm2_,fmm2_nomi_);
  fHisto->FillHisto("K2PINORM_mmiss_vs_zvtx_0",fvtx_.Z(), fmm2_);
  fHisto->FillHisto("K2PINORM_p_vs_zvtx_0",fvtx_.Z(), fp_);
  fHisto->FillHisto("K2PINORM_cda_vs_dt_0",fdt_kk_,fcda_);
  fHisto->FillHisto("K2PINORM_downstream_distance_0",fpion_->GetDownstreamDistance());


  if(!fgtk_match_) return 0;
  // if(fabs(fdt_kk_) > 0.6) return 0;
  // if(fabs(fkaon_->GetTGTK()-fpion_->GetTCHOD()) > 1.1) return 0;
  if(fkaon_->GetGTKExtraHits()) return 0;
  if(!frich_pid_) return 0;
  // BeamBackgroundCut
  if(fbb_cut_BDTTrees) return 0;
  if(fevt_->GetIsBroadMultiVertex()) return 0;

  if (!fcalo_pid_) return 0;

  //if(fgtk_cut_ && fellipse_ )// ne cut on dtij in GTK (to be revised)
  TriggerAnalysis();

  //!Background and Control region K2PI definition
  bool isBGR      = fmm2_ > 0.01 && fmm2_ <= 0.026;
  bool isBGR_RICH = fmm2_rich_ > -0.04 && fmm2_rich_ <= 0.07;


  if(!fp_region_) return 0;


  if(isBGR && isBGR_RICH){
      fHisto->FillHisto("K2PINORM_NORM_mmiss_vs_p_1",fp_,fmm2_);
      fHisto->FillHisto("K2PINORM_NORM_run_vs_burst_1",fevt_->GetBurstNumber(),fevt_->GetRunNumber());
      fHisto->FillHisto("K2PINORM_NORM_dtkpi_vs_dtkk_1", fdt_kk_, fdt_kpi_);


  }

  int bin = -1;
  if(fp_ <= 15 || fp_ > 40) return 0;
  if(fp_ <= 18 && fp_ > 15) bin = 1;
  if(fp_ <= 25 && fp_ > 18) bin = 2;
  if(fp_ <= 32 && fp_ > 25) bin = 3;
  if(fp_ <= 35 && fp_ > 32) bin = 4;
  if(fp_ <= 40 && fp_ > 35) bin = 5;

  fHisto->FillHisto("K2PINORM_lambda_1",flambda_);
  fHisto->FillHisto("K2PINORM_mmiss_vs_p_1",fp_,fmm2_);
  fHisto->FillHisto("K2PINORM_mmiss_vs_mmissRICH_1",fmm2_,fmm2_rich_);
  fHisto->FillHisto("K2PINORM_mmiss_vs_mmissNominalK_1",fmm2_,fmm2_nomi_);
  fHisto->FillHisto("K2PINORM_mmiss_vs_zvtx_1",fvtx_.Z(), fmm2_);
  fHisto->FillHisto("K2PINORM_p_vs_zvtx_1",fvtx_.Z(), fp_);
  fHisto->FillHisto("K2PINORM_cda_vs_dt_1",fdt_kk_,fcda_);
  fHisto->FillHisto("K2PINORM_downstream_distance_1",fpion_->GetDownstreamDistance());



  if(fregion_ != 1 && fregion_ != 2 && fregion_ != 10 && fregion_ != 20 && fregion_ != 100) return 0;

  fHisto->FillHisto(Form("C%d_rstraw1_vs_zvtx", bin),fvtx_.Z(),fstraw1_r_);
  fHisto->FillHisto(Form("C%d_trim5y_vs_trim5x", bin),ftrim5_x_,ftrim5_y_);
  fHisto->FillHisto(Form("K2PINORM_KINE_r%d_mmiss_vs_mmissRICH_bin%d",fregion_,bin),fmm2_, fmm2_rich_);

  if(!fIsInTime) return 0;
  if(!fIsNormRegion) return 0;
  if(!fFlagSelectionForBDT && (fbox_cut_ || fbb_cut_) ) return 0; // standard bb cuts (without fmult_tot_)
  if(!(fbox_cut_ || fbb_cut_)){
    fHisto->FillHisto("K2PiNorm_StdBBcuts_mmiss_vs_p",fp_,fmm2_);
    fHisto->FillHisto("K2PiNorm_StdBBcuts_Zvtx_vs_p",fp_,0.001*fvtx_.Z());
  }
  if(fFlagSelectionForBDT && fTMVAUpstreamCut) return 0;
  fHisto->FillHisto("K2PiNorm_mmiss_vs_p",fp_,fmm2_);
  fHisto->FillHisto("K2PiNorm_Zvtx_vs_p",fp_,0.001*fvtx_.Z());

  return 1;
}


void K2piNormAnal::TriggerAnalysis() {
  if(!fpipi_region_) return;
  if(!ftrig_->GetIsTrigger()) return;
  //Most of the PNN mask components except the LKr
  if(fp_region_ && !fevt_->GetIsMultiplicity()){
    if(ftrig_->GetRICHRef()){
      bool L0PNN = !ftrig_->GetMUV3L0() && ftrig_->GetNewCHODL0() && !ftrig_->GetQxL0() && ftrig_->GetUTMCL0();

      fHisto->FillHisto("mmiss_vs_p_trig",fp_,fevt_->GetMMiss());
      fHisto->FillHisto("L0MUV3"     , fp_,  ftrig_->GetMUV3L0());
      fHisto->FillHisto("L0NewCHOD"  , fp_,  ftrig_->GetNewCHODL0());
      fHisto->FillHisto("L0QX"       , fp_,  ftrig_->GetQxL0());
      fHisto->FillHisto("L0UTMC"     , fp_,  ftrig_->GetUTMCL0());
      fHisto->FillHisto("L0TOT_NOLKR", fp_,  L0PNN);

      fHisto->FillHisto("Lambda_L0MUV3"     , flambda_,  ftrig_->GetMUV3L0());
      fHisto->FillHisto("Lambda_L0NewCHOD"  , flambda_,  ftrig_->GetNewCHODL0());
      fHisto->FillHisto("Lambda_L0QX"       , flambda_,  ftrig_->GetQxL0());
      fHisto->FillHisto("Lambda_L0UTMC"     , flambda_,  ftrig_->GetUTMCL0());
      fHisto->FillHisto("Lambda_L0TOT_NOLKR", flambda_,  L0PNN);
    }

    if(ftrig_->GetNewCHODRef()){

      fHisto->FillHisto("L0RICH"     , fp_,  ftrig_->GetRICHL0());
      fHisto->FillHisto("Lambda_L0RICH"     , flambda_,  ftrig_->GetRICHL0());
    }

  }

  //LKr component of the L0 PNN mask
  //bool PhotonLight = fevt_->GetIsSignalInIRC() || fevt_->GetIsSignalInSAC() || fevt_->GetIsSignalInSAV() ;
  bool PhotonLight = fevt_->GetIsSignalInIRC() || fevt_->GetIsSignalInSAC() || fevt_->GetIsSignalInSAV() || fevt_->GetIsPhotonInLKr() || fevt_->GetIsAuxiliaryLKr() ;
  bool Photon = fevt_->GetIsSignalInIRC() || fevt_->GetIsSignalInSAC() || fevt_->GetIsSignalInSAV() || fevt_->GetIsPhotonInLKr() || fevt_->GetIsAuxiliaryLKr() || fhit_mult_ || fevt_->GetIsSegment() || !fevt_->GetIsOneParticle();
  if(fevt_->GetIs2PhotonsInLAV()){
    fHisto->FillHisto("mmiss_vs_p_trig_2lav",fp_,fevt_->GetMMiss());
    if(!PhotonLight){
      fHisto->FillHisto("mmiss_vs_p_trig_2lav_light",fp_,fevt_->GetMMiss());

      if(!fhit_mult_){
          fHisto->FillHisto("mmiss_vs_p_trig_2lav_light1",fp_,fevt_->GetMMiss());
          if(!fevt_->GetIsSegment() && fevt_->GetIsOneParticle())
              fHisto->FillHisto("mmiss_vs_p_trig_2lav_light2",fp_,fevt_->GetMMiss());
      }
      if(!Photon){

        if(ftrig_->GetRICHRef()){
          fHisto->FillHisto("mmiss_vs_p_trig_2lav_end",fp_,fevt_->GetMMiss());
          fHisto->FillHisto("Lambda_vs_p",fp_, flambda_);
          fHisto->FillHisto("Lambda_L0LKr",fpion_->GetLKrEnergy(), flambda_);
          fHisto->FillHisto("L0LKr"     , fpion_->GetLKrEnergy(),  ftrig_->GetLKrL0());
        }
      }
    }
  }
  return;
}

void K2piNormAnal::ResetEvent() {

  fevt_  = NULL;
  fpion_ = NULL;
  fkaon_ = NULL;

  fvtx_      = TVector3(0.,0.,0.);
  fmm2_      = -99999;
  fmm2_rich_ = -99999;
  fmm2_nomi_ = -99999;
  fp_        = -99999;
  ftrim5_x_  = -99999;
  ftrim5_y_  = -99999;
  ftrim5_r_  = -99999;
  flambda_   = -99999;
  fcda_      = -99999;

  fgtk_match_= 0.;
  fdt_kk_    = -99999;
  fdt_kpi_   = -99999;

  frich_pid_ = 0;
  fmax_lh_   = -99999;
  frich_mass_range_ = 0;
  fsnake_    = 0;
  fpipi_region_ = 0;
  fp_region_ = 0;
  fregion_ = 0;

  fphoton_   = 0;
  fhit_mult_ = 0;
  fmult_tot_ = 0;
  fbox_cut_  = 0;
}

bool K2piNormAnal::IsSnake() {

  double zvtx = fvtx_.Z();
  double b1 = -0.004;
  double a1 = 315-b1*105000.;
  double r115 = a1+b1*115000.;
  double b2 = -(900-r115)/10000.;
  double a2 = 900-b2*105000.;
  double b3 = -0.00983333333;
  double a3 = 780-b3*105000.;
  double cut1 = a1+b1*zvtx;
  double cut2 = a2+b2*zvtx;
  double cut3 = a3+b3*zvtx;
  bool isSignal = fstraw1_r_>cut1 && fstraw1_r_>cut2 && fstraw1_r_<cut3 && zvtx<165000.;
  return !isSignal;
}

void K2piNormAnal::DiscriminantNormalization() {

  double pass_dt = 0.001; // mm

  fPDFKaonDT_.clear();
  fIntPDFKaonDT_ = 0;
  for (int jbindt(0); jbindt<3000; jbindt++) { // +-3 ns
    double dt = pass_dt*jbindt+0.5*pass_dt;
    fIntPDFKaonDT_ += (PDFKaonDT(dt)+PDFKaonDT(-dt))*pass_dt/0.01; // compute the total integral of the PDF
    fPDFKaonDT_.push_back(fIntPDFKaonDT_); // fill the cumulative distribution function
  }
}

Double_t K2piNormAnal::Discriminant(const Double_t &dt_val) {
  double D = 0;

  if(fabs(dt_val) >= 3) return D;

  double pkdt    = 0;
  double pass_dt = 0.001;
  for (auto ikdt = fPDFKaonDT_.begin(); ikdt != fPDFKaonDT_.end();++ikdt) {
    double id = (double)(distance(fPDFKaonDT_.begin(),ikdt))*pass_dt; // compute the time corresponding to each element of the cumulative distribution function
    if(fabs(dt_val) < id) {
      pkdt = id/fIntPDFKaonDT_;

      break;
    }
  }

  D = (1 - pkdt);
  return D;
}
Double_t K2piNormAnal::PDFKaonDT(const Double_t &dt) {
  Double_t dtpar[3];

  dtpar[0] = 0.02;
  dtpar[1] = 0.;
  dtpar[2] = 0.2;

  Double_t dtg1 = dtpar[0]*exp(-0.5*((dt-dtpar[1])/dtpar[2])*((dt-dtpar[1])/dtpar[2]));
  return dtg1;
}

int K2piNormAnal::GetRegion() {
  bool isPNNRegion1 = 0;
  double bound = Kmu2KinematicBound();
  if (fp_<=20.)
    isPNNRegion1 = (fmm2_>0.    && fmm2_<=0.01)  && (fmm2_nomi_>-0.005&&fmm2_nomi_<=0.0135) && (fmm2_rich_>0.    &&fmm2_rich_<=0.01);
  if (fp_>20. && fp_<=25.)
    isPNNRegion1 = (fmm2_>0.    && fmm2_<=0.01)  && (fmm2_nomi_>-0.005&&fmm2_nomi_<=0.0135) && (fmm2_rich_>0.    &&fmm2_rich_<=0.02);
  if (fp_>25.)
    isPNNRegion1 = (fmm2_>0.    && fmm2_<=0.01)  && (fmm2_nomi_>0.    &&fmm2_nomi_<=0.0135) && (fmm2_rich_>-0.005&&fmm2_rich_<=0.02);

  bool isPNNRegion2 = (fmm2_>0.026 && fmm2_<=0.068) && (fmm2_nomi_>0.024 &&fmm2_nomi_<=0.068)  && (fmm2_rich_>0.02  &&fmm2_rich_<=0.07);
  bool isCR1        = fmm2_>0.01  && fmm2_<=0.015;
  bool isCR2        = fmm2_>0.021 && fmm2_<=0.026;
  bool isCRmu       = fmm2_<=0     && fmm2_>bound+0.0036;
  bool isCR3pi      = fmm2_>0.068 && fmm2_<=0.072;
  bool isCRUp       = (fmm2_>bound-0.0070 && fmm2_<=bound-0.0060);
  bool isK2piRegion = (fmm2_>0.015 && fmm2_<=0.021);
  bool isKmu2Region = /*(fmm2_>-0.05 && fmm2_<bound+0.0036)*/(fmm2_>bound-0.0060 && fmm2_<= bound+0.0036);
  bool isK3PiRegion  = (fmm2_>0.072);
  bool isUpRegion   = fmm2_<=bound-0.0070;

  if (isPNNRegion1) return 1;
  if (isPNNRegion2) return 2;
  if (isCR1) return 10;
  if (isCR2) return 20;
  if (isCRmu) return 30;
  if (isCR3pi) return 40;
  if (isCRUp) return 50;
  if (isK2piRegion) return 100;
  if (isKmu2Region) return 200;
  if (isK3PiRegion) return 300;
  if (isUpRegion) return 400;


  return 0;
}

double K2piNormAnal::Kmu2KinematicBound(){

    double mpi = 0.13957018;
    double mmu = 0.1056583745;

    return (pow(mpi,2) - pow(mmu,2))*(1 - 75/fp_);
}

int K2piNormAnal::PionPID() {

  bool D = 0;

  if (fpion_->GetIsMUV3()) D = 1;
  if (fpion_->GetIsMulti()) D = 1;
  if (fpion_->GetIsMip()) D = 1;

  Double_t caloEnergy = fpion_->GetCaloEnergy();
  Double_t r1 = fpion_->GetMUV1Energy()/caloEnergy;
  Double_t r2 = fpion_->GetMUV2Energy()/caloEnergy;
  Double_t elkr = fpion_->GetLKrEnergy();
  Double_t seedratio = fpion_->GetLKrSeedEnergy()/elkr;
  Double_t cellratio = fpion_->GetLKrNCells()/elkr;
  bool isLKrEM = 0;
  auto vcut       = TMath::Max(0.7,0.98-0.4596*exp(-(fp_-11.44)/5.27));
  bool IsCaloPion = fpion_->GetCaloPionProb()>=vcut;

  if (r1<0.01 && r2<0.01) {
    if (seedratio>0.2&&cellratio<=3) isLKrEM = 1;
    if (seedratio<=0.2&&cellratio<1.8) isLKrEM = 1;
    if (seedratio>0.35) isLKrEM = 1;
    if (seedratio<0.05) isLKrEM = 1;
  }
  if (seedratio>0. && seedratio<0.8 && cellratio<1.4) isLKrEM = 1;

  if(!IsCaloPion) D = 1;
  if (isLKrEM) D = 1;
  if (caloEnergy>1.2*fp_) D = 1;
  if (elkr>0.8*fp_) D = 1;
  if (!fpion_->GetIsGoodMUV1() && fpion_->GetIsGoodMUV2()) D = 1;

  return D > 0 ? 0 : 1;
}
