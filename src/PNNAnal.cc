#include <stdlib.h>
#include <iostream>
#include "PNNAnal.hh"
#include <bitset>

// trees variables for BDT training
Float_t fp_tree = 999999999.9;
Float_t fmm2_tree = 999999999.9;
Float_t Xtrim5_tree = 999999999.9;
Float_t Ytrim5_tree = 999999999.9;
Float_t Xstraw1_tree = 999999999.9;
Float_t Ystraw1_tree = 999999999.9;
Float_t Rstraw1_tree = 999999999.9;
Float_t Xvtx_tree = 999999999.9;
Float_t Yvtx_tree = 999999999.9;
Float_t Zvtx_tree = 999999999.9;
Float_t cda_tree = 999999999.9;
Float_t ThetaX_tree = 999999999.9;
Float_t ThetaY_tree = 999999999.9;
Float_t Lambda_tree = 999999999.9;

PNNAnal::PNNAnal() {
    DiscriminantNormalization();
}

PNNAnal::~PNNAnal() {
  delete fIMin;
  delete fIMax;
}

void PNNAnal::SetParameter() {
  
}

void PNNAnal::InitCounter() {
  
  fFlagSelectionForBDT = true; // false to use cut-based selection
  fFlagReaderBDT = true;
  fFlagFillBDTTrees = false;

  if(fFlagFillBDTTrees){
    fTreeSig = new TTree("fTreeSig","Sig");
    fTreeSig->Branch("Ptrack",&fp_tree,"fp_tree/F");
    fTreeSig->Branch("M2Miss",&fmm2_tree,"fmm2_tree/F");
    fTreeSig->Branch("Xtrim5",&Xtrim5_tree,"Xtrim5_tree/F");
    fTreeSig->Branch("Ytrim5",&Ytrim5_tree,"Ytrim5_tree/F");
    fTreeSig->Branch("Xstraw1",&Xstraw1_tree,"Xstraw1_tree/F");
    fTreeSig->Branch("Ystraw1",&Ystraw1_tree,"Ystraw1_tree/F");
    fTreeSig->Branch("Rstraw1",&Rstraw1_tree,"Rstraw1_tree/F");
    fTreeSig->Branch("Xvtx",&Xvtx_tree,"Xvtx_tree/F");
    fTreeSig->Branch("Yvtx",&Yvtx_tree,"Yvtx_tree/F");
    fTreeSig->Branch("Zvtx",&Zvtx_tree,"Zvtx_tree/F");
    fTreeSig->Branch("cda",&cda_tree,"cda_tree/F");
    fTreeSig->Branch("ThetaX",&ThetaX_tree,"ThetaX_tree/F");
    fTreeSig->Branch("ThetaY",&ThetaY_tree,"ThetaY_tree/F");
    fTreeSig->Branch("Lambda",&Lambda_tree,"Lambda_tree/F");
    fTreeBkg = new TTree("fTreeBkg","Bkg");
    fTreeBkg->Branch("Ptrack",&fp_tree,"fp_tree/F");
    fTreeBkg->Branch("M2Miss",&fmm2_tree,"fmm2_tree/F");
    fTreeBkg->Branch("Xtrim5",&Xtrim5_tree,"Xtrim5_tree/F");
    fTreeBkg->Branch("Ytrim5",&Ytrim5_tree,"Ytrim5_tree/F");
    fTreeBkg->Branch("Xstraw1",&Xstraw1_tree,"Xstraw1_tree/F");
    fTreeBkg->Branch("Ystraw1",&Ystraw1_tree,"Ystraw1_tree/F");
    fTreeBkg->Branch("Rstraw1",&Rstraw1_tree,"Rstraw1_tree/F");
    fTreeBkg->Branch("Xvtx",&Xvtx_tree,"Xvtx_tree/F");
    fTreeBkg->Branch("Yvtx",&Yvtx_tree,"Yvtx_tree/F");
    fTreeBkg->Branch("Zvtx",&Zvtx_tree,"Zvtx_tree/F");
    fTreeBkg->Branch("cda",&cda_tree,"cda_tree/F");
    fTreeBkg->Branch("ThetaX",&ThetaX_tree,"ThetaX_tree/F");
    fTreeBkg->Branch("ThetaY",&ThetaY_tree,"ThetaY_tree/F");
    fTreeBkg->Branch("Lambda",&Lambda_tree,"Lambda_tree/F");
  }
  
  // BDT reader
  if(fFlagReaderBDT){
    const char* inputVars[] = {"Xtrim5","Ytrim5","Xstraw1","Ystraw1","Rstraw1","Xvtx","Yvtx","Zvtx","ThetaX","ThetaY"};
    fTMVAReader = new TMVA::Reader("Verbose",0);
    for (int i=0; i<10; i++) fTMVAReader->AddVariable(TString(inputVars[i]),&fTMVAVariables[i]);
    fTMVAReader->BookMVA("myBDT","/afs/cern.ch/work/f/fbriziol/public/BDTweights.xml");
  }
}

void PNNAnal::SetupVariables() {

    ResetEvent();

    fevt_  = GetEvent();
    fpion_ = (PnnNA62DownstreamParticle *)fevt_->GetDownstreamParticle();
    fkaon_ = (PnnNA62UpstreamParticle *)fevt_->GetUpstreamParticle();
    ftrig_ = (PnnNA62Trigger *)fevt_->GetTrigger();

    fRunNumber = fevt_->GetRunNumber();
    // fBurstNumber = fevt_->GetBurstNumber();
    // fIsMC = GetIsMC();
    fIsMC = false;
    if(fRunNumber<8500) fIsMC = true; // it works only with MC generated with 2017 runs as reference!

    fNCategories = 10;
    //Kinematic variables + instantaneous intensity
    fvtx_      = fevt_->GetVertex();
    fmm2_      = fevt_->GetMMiss();
    fmm2_rich_ = fevt_->GetMMissRich();
    fmm2_nomi_ = fevt_->GetMMissNominalK();
    fp_        = fpion_->GetP().Mag();
    ftrim5_x_  = fpion_->GetPosTrim5X();
    ftrim5_y_  = fpion_->GetPosTrim5Y();
    ftrim5_r_  = sqrt(pow(ftrim5_x_,2) + pow(ftrim5_y_, 2));
    fstraw1_x_ = fpion_->GetPosStraw1X();
    fstraw1_y_ = fpion_->GetPosStraw1Y();
    fstraw1_r_ = sqrt( pow(fstraw1_x_ - 101.2, 2) + pow(fstraw1_y_, 2));
    flambda_   = fevt_->GetLambda();
    fcda_      = fevt_->GetCDA();
    fthx_      = fpion_->GetP().X()/fpion_->GetP().Z();
    fthy_      = fpion_->GetP().Y()/fpion_->GetP().Z();

    fbox_cut_  = fabs(ftrim5_x_)<100. && fabs(ftrim5_y_)<100.;

    // fTMVACutValue = 0.577484; // old
    fTMVACutValue = 0.631603 ;
    fTMVAUpstreamCut = true;
    // BDT reader
    if(fFlagReaderBDT){
      fTMVAVariables[0] = ftrim5_x_ ;
      fTMVAVariables[1] = ftrim5_y_ ;
      fTMVAVariables[2] = fstraw1_x_ ;
      fTMVAVariables[3] = fstraw1_y_ ;
      fTMVAVariables[4] = fstraw1_r_ ;
      fTMVAVariables[5] = fvtx_.X() ;
      fTMVAVariables[6] = fvtx_.Y() ;
      fTMVAVariables[7] = fvtx_.Z() ;
      fTMVAVariables[8] = fthx_ ;
      fTMVAVariables[9] = fthy_ ;
      fTMVAValue = fTMVAReader->EvaluateMVA("myBDT");
      if(fTMVAValue>fTMVACutValue) fTMVAUpstreamCut = false; // don't cut
    }

    //GTK timing variables and related cuts
    //if (fevt_->GetIsRICHBeamDiscriminant()>0.03)
    if ((fevt_->GetIsKTAGBeamDiscriminant()>0.03 || fevt_->GetIsRICHBeamDiscriminant()>0.03) && fevt_->GetIsKTAGBeamDiscriminant() > 0.005 && fevt_->GetIsRICHBeamDiscriminant() > 0.005)
        fgtk_match_ = true;

    fdt_kk_    = fkaon_->GetTGTK() -fkaon_->GetTKTAG();
    fdt_kpi_   = fkaon_->GetTKTAG() - fpion_->GetTRICH();

    fIsInTime = true ;
    if(fabs(fdt_kk_)>0.5 || fabs(fdt_kpi_)>0.5) fIsInTime = false;

    //PID variables
    for (int j=0; j<5; j++) {
        if (j==3) continue;
        if (fpion_->GetRICHLikeProb(j)> fmax_lh_) fmax_lh_ = fpion_->GetRICHLikeProb(j);
    }
    frich_mass_range_ = fpion_->GetRICHMass()>0.125&&fpion_->GetRICHMass()<0.2 ? true : false;
    if (fmax_lh_<=0.12 && frich_mass_range_) frich_pid_ = true;

    fcalo_pid_    = PionPID();

    //Beam background variables
    fsnake_       = IsSnake();
    fbb_cut_      = fsnake_ || fevt_->GetIsSignalInCHANTI() || fevt_->GetIsInteraction() || fkaon_->GetNGTKTracks()>5 || fvtx_.Z() <= 105000 || fvtx_.Z() > 165000;
    fbb_cut_BDTTrees = fevt_->GetIsSignalInCHANTI() || fevt_->GetIsInteraction() || fkaon_->GetNGTKTracks()>5 || fvtx_.Z() <= 105000 || fvtx_.Z() > 175000 ;
    //Region definitions
    fregion_      = GetRegion(0);
    fIsSignalRegions = false;
    if(fregion_==1 && fp_>15 && fp_<35) fIsSignalRegions = true;
    if(fregion_==2 && fp_>15 && fp_<35) fIsSignalRegions = true;

    fBlindCut = false;
    if(!fIsMC && fregion_!=100 && fregion_!=200 && fregion_!=300 && fregion_!=400) fBlindCut = true;

    //PhotonRejection
    fphoton_      = fevt_->GetIsSignalInLAV() || fevt_->GetIsSignalInSAC() || fevt_->GetIsSignalInIRC() || fevt_->GetIsSignalInSAV() || fevt_->GetIsPhotonInLKr() || fevt_->GetIsAuxiliaryLKr()|| fevt_->GetIsLKrInTimeHits();
    fhit_mult_    = fevt_->GetMultAllOldCH7() > 3 ||fevt_->GetMultOldNewCH() > 0 || fevt_->GetMultNewCHLKr() > 0 || fevt_->GetMultOldCHLKr() > 0 || fevt_->GetMultHAC() == 1 || fevt_->GetMultMUV0() == 1 || (fevt_->GetMultLKrMerged() && fevt_->GetMultAllOldCH7() > 0); // cut Rstraw1 vs Zvtx removed with respect to the full fevt_->GetIsMultiplicity()
    fmult_tot_    = fevt_->GetIsMultiplicity() || fevt_->GetIsSegment() || !fevt_->GetIsOneParticle() || fevt_->GetIsBroadMultiVertex();
    fmult_tot_BDTTrees = fhit_mult_ || fevt_->GetIsSegment() || !fevt_->GetIsOneParticle() || fevt_->GetIsBroadMultiVertex();

    return;
}
/////////////////////////
//// USER ANALYSIS //////
/////////////////////////

bool PNNAnal::EventAnalysis() { // "process"
    SetupVariables();
    FullPinunuAnalysis();
    UpstreamBackgroundAnalysis();

    return 1;
}
void PNNAnal::ResetEvent() {

    fevt_  = NULL;
    fpion_ = NULL;
    fkaon_ = NULL;
    ftrig_ = NULL;

    fvtx_      = TVector3(0.,0.,0.);
    fmm2_      = -99999;
    fmm2_rich_ = -99999;
    fmm2_nomi_ = -99999;
    fp_        = -99999;
    ftrim5_x_  = -99999;
    ftrim5_y_  = -99999;
    ftrim5_r_  = -99999;
    fstraw1_x_  = -99999;
    fstraw1_y_  = -99999;
    fstraw1_r_  = -99999;
    flambda_   = -99999;
    fcda_      = -99999;
    fbox_cut_  = 0;

    fgtk_match_= 0.;
    fdt_kk_     = -99999;
    fdt_kpi_    = -99999;

    fcalo_pid_ = 0;
    frich_pid_ = 0;
    fmax_lh_   = -99999;
    frich_mass_range_ = 0;
    fsnake_    = 0;
    fregion_   = 0;
    fbb_cut_   = 0;
    fbb_cut_BDTTrees   = 0;
    fphoton_   = 0;
    fhit_mult_ = 0;
    fmult_tot_ = 0;
    fmult_tot_BDTTrees = 0;
    fTMVAValue = 999999999.9;
    fTMVAUpstreamCut = true;

    //Categories-related variables
    fNCategories = 0;
    fpid_.clear();
    fk_pi_match_.clear();
    fpi0_rej_.clear();
    fkin_cuts_.clear();

}

///////
//// -> Add Segment !!!
int PNNAnal::FullPinunuAnalysis() {
  if(fBlindCut) return 0; // blinding signal and control regions

    fHisto->FillHisto("PNNMain_mmiss_vs_p_0",fp_,fmm2_);
    fHisto->FillHisto("PNNMain_mmiss_vs_mmissRICH_0",fmm2_,fmm2_rich_);
    fHisto->FillHisto("PNNMain_mmiss_vs_mmissNominalK_0",fmm2_,fmm2_nomi_);
    fHisto->FillHisto("PNNMain_mmiss_vs_zvtx_0",fvtx_.Z(), fmm2_);
    fHisto->FillHisto("PNNMain_rstraw1_vs_zvtx_0",fvtx_.Z(), fstraw1_r_);
    fHisto->FillHisto("PNNMain_y_vs_x_trim5_0",ftrim5_x_, ftrim5_y_);
    fHisto->FillHisto("PNNMain_p_vs_zvtx_0",fvtx_.Z(), fp_);
    fHisto->FillHisto("PNNMain_cda_vs_dt_0",fdt_kk_,fcda_);

    //STRAW-GTK matching
    if(!fgtk_match_) return 0;
    // if(fabs(fdt_kk_) > 0.6) return 0; // applied later using both fdt_kk_ and fdt_kpi_
    if(fabs(fkaon_->GetTGTK()-fpion_->GetTCHOD()) > 1.1) return 0;
    if(fkaon_->GetGTKExtraHits()) return 0;

    //Beam Background cut
    if (fbb_cut_BDTTrees) return 0;
    if(fevt_->GetIsBroadMultiVertex()) return 0; // also included in fmult_tot_BDTTrees

    //Particle identification: Calo
    if (!fcalo_pid_) return 0;

    //Pi0 rejection
    if(fphoton_) return 0;
    if(fhit_mult_) return 0; // also included in fmult_tot_BDTTrees

    bool RKE4 = fp_ > 15 && fp_ <= 35 && fmm2_ <= 0.071 && fmm2_ > 0.03;

    if ((fevt_->GetIsSegment() || !fevt_->GetIsOneParticle()) && !fbox_cut_ && !fphoton_ && frich_pid_){
        if(RKE4){
            fHisto->FillHisto("PNNMain_BIF_lambda",flambda_);
        }
        fHisto->FillHisto("PNNMain_BIF_mmiss_vs_p",fp_,fmm2_);
        fHisto->FillHisto("PNNMain_BIF_mmiss_vs_mmissRICH",fmm2_,fmm2_rich_);
        fHisto->FillHisto("PNNMain_BIF_mmiss_vs_mmissNominalK",fmm2_,fmm2_nomi_);
        fHisto->FillHisto("PNNMain_BIF_mmiss_vs_zvtx",fvtx_.Z(), fmm2_);
        fHisto->FillHisto("PNNMain_BIF_p_vs_zvtx",fvtx_.Z(), fp_);
        fHisto->FillHisto("PNNMain_BIF_cda_vs_dt",fdt_kk_,fcda_);
        fHisto->FillHisto("PNNMain_BIF_rstraw1_vs_zvtx",fvtx_.Z(), fstraw1_r_);
        fHisto->FillHisto("PNNMain_BIF_y_vs_x_trim5",ftrim5_x_, ftrim5_y_);
        fHisto->FillHisto("PNNMain_BIF_downstream_distance",fpion_->GetDownstreamDistance());

    }

    //Complete multiplicity cut + segments + straw mult + broad multi vtx cuts
    if (fmult_tot_BDTTrees) return 0;

    if(fp_ <= 5 || fp_ > 40) return 0;

    fHisto->FillHisto("PNNMain_mmiss_vs_p_1",fp_,fmm2_);
    fHisto->FillHisto("PNNMain_mmiss_vs_mmissRICH_1",fmm2_,fmm2_rich_);
    fHisto->FillHisto("PNNMain_mmiss_vs_mmissNominalK_1",fmm2_,fmm2_nomi_);
    fHisto->FillHisto("PNNMain_mmiss_vs_zvtx_1",fvtx_.Z(), fmm2_);
    fHisto->FillHisto("PNNMain_rstraw1_vs_zvtx_1",fvtx_.Z(), fstraw1_r_);
    fHisto->FillHisto("PNNMain_y_vs_x_trim5_1",ftrim5_x_, ftrim5_y_);
    fHisto->FillHisto("PNNMain_p_vs_zvtx_1",fvtx_.Z(), fp_);
    fHisto->FillHisto("PNNMain_cda_vs_dt_1",fdt_kk_,fcda_);
    fHisto->FillHisto("PNNMain_downstream_distance_1",fpion_->GetDownstreamDistance());
    fHisto->FillHisto("PNNMain_dtkpi_vs_dtkk_1",fdt_kk_,fdt_kpi_);

    fHisto->FillHisto(Form("PNNMain_r%d_mmiss_vs_p", fregion_),fp_,fmm2_);
    fHisto->FillHisto(Form("PNNMain_r%d_lambda_vs_p", fregion_),flambda_,fp_);
    fHisto->FillHisto(Form("PNNMain_r%d_mmiss_vs_mmissNominalK", fregion_),fmm2_, fmm2_nomi_);
    fHisto->FillHisto(Form("PNNMain_r%d_mmiss_vs_mmissRICH", fregion_),fmm2_, fmm2_rich_);

    //Particle identification: RICH
    if(!frich_pid_) return 0;

    //Test the weird component
    if (fevt_->GetIsHighToT() || fevt_->GetIsSignalInCHANTI() || fevt_->GetIsInteraction()){
        fHisto->FillHisto("PNNMain_mmiss_vs_p_2",fp_,fmm2_);
        fHisto->FillHisto("PNNMain_mmiss_vs_mmissRICH_2",fmm2_,fmm2_rich_);
        fHisto->FillHisto("PNNMain_mmiss_vs_mmissNominalK_2",fmm2_,fmm2_nomi_);
        fHisto->FillHisto("PNNMain_mmiss_vs_zvtx_2",fvtx_.Z(), fmm2_);
        fHisto->FillHisto("PNNMain_rstraw1_vs_zvtx_2",fvtx_.Z(), fstraw1_r_);
        fHisto->FillHisto("PNNMain_y_vs_x_trim5_2",ftrim5_x_, ftrim5_y_);
        fHisto->FillHisto("PNNMain_p_vs_zvtx_2",fvtx_.Z(), fp_);
        fHisto->FillHisto("PNNMain_cda_vs_dt_2",fdt_kk_,fcda_);
        fHisto->FillHisto("PNNMain_downstream_distance_2",fpion_->GetDownstreamDistance());
        fHisto->FillHisto("PNNMain_dtkpi_vs_dtkk_2",fdt_kk_,fdt_kpi_);
    }
    if(fp_ <= 10 || fp_ > 40) return 0;

    fHisto->FillHisto("PNNMain_mmiss_vs_p_3",fp_,fmm2_);
    fHisto->FillHisto("PNNMain_mmiss_vs_mmissRICH_3",fmm2_,fmm2_rich_);
    fHisto->FillHisto("PNNMain_mmiss_vs_mmissNominalK_3",fmm2_,fmm2_nomi_);
    fHisto->FillHisto("PNNMain_mmiss_vs_zvtx_3",fvtx_.Z(), fmm2_);
    fHisto->FillHisto("PNNMain_rstraw1_vs_zvtx_3",fvtx_.Z(), fstraw1_r_);
    fHisto->FillHisto("PNNMain_y_vs_x_trim5_3",ftrim5_x_, ftrim5_y_);
    fHisto->FillHisto("PNNMain_p_vs_zvtx_3",fvtx_.Z(), fp_);
    fHisto->FillHisto("PNNMain_cda_vs_dt_3",fdt_kk_,fcda_);
    fHisto->FillHisto("PNNMain_downstream_distance_3",fpion_->GetDownstreamDistance());
    fHisto->FillHisto("PNNMain_dtkpi_vs_dtkk_3",fdt_kk_,fdt_kpi_);
    int category = GetCategory();

    if(category!=0){
        fHisto->FillHisto(Form("C%d_rstraw1_vs_zvtx", category),fvtx_.Z(),fstraw1_r_);
        fHisto->FillHisto(Form("C%d_trim5y_vs_trim5x", category),ftrim5_x_,ftrim5_y_);
    }

    int bin = -1;
    if(fp_ <= 18 && fp_ > 15) bin = 1;
    if(fp_ <= 25 && fp_ > 18) bin = 2;
    if(fp_ <= 32 && fp_ > 25) bin = 3;
    if(fp_ <= 35 && fp_ > 32) bin = 4;
    if(fp_ <= 40 && fp_ > 35) bin = 5;
    if(bin >= 1 && bin <= 5){
        fHisto->FillHisto(Form("C%d_rstraw1_vs_zvtx", bin),fvtx_.Z(),fstraw1_r_);
        fHisto->FillHisto(Form("C%d_trim5y_vs_trim5x", bin),ftrim5_x_,ftrim5_y_);
    }

    if(fFlagSelectionForBDT && fIsInTime && fIsSignalRegions){ // flag isMC should be needed
      if(fFlagFillBDTTrees){
        fp_tree = fp_ ;
        fmm2_tree = fmm2_ ;
        Xtrim5_tree = ftrim5_x_ ;
        Ytrim5_tree = ftrim5_y_ ;
        Xstraw1_tree = fstraw1_x_ ;
        Ystraw1_tree = fstraw1_y_ ;
        Rstraw1_tree = fstraw1_r_ ;
        Xvtx_tree = fvtx_.X() ;
        Yvtx_tree = fvtx_.Y() ;
        Zvtx_tree = fvtx_.Z() ;
        cda_tree = fcda_ ;
        ThetaX_tree = fthx_ ;
        ThetaY_tree = fthy_ ;
        Lambda_tree = flambda_ ;
        fTreeSig->Fill();
        fHisto->FillHisto("BDT_sig_mmiss_vs_p_Training",fp_,fmm2_);
        if(!(fbox_cut_ || fbb_cut_ || fmult_tot_)) fHisto->FillHisto("BDT_sig_mmiss_vs_p_TrainingAfterStandardCuts",fp_,fmm2_);
      }
      if(fFlagReaderBDT){
        fHisto->FillHisto("BDT_sig_Classifier",fTMVAValue);
        if(!(fbox_cut_ || fbb_cut_ || fmult_tot_)){
	  fHisto->FillHisto("BDT_sig_Classifier_AfterStandardCuts",fTMVAValue);
	  fHisto->FillHisto("BDT_sig_mmiss_vs_p_AfterStandardCuts",fp_,fmm2_);
	  fHisto->FillHisto("BDT_sig_Zvtx_vs_p_AfterStandardCuts",fp_,0.001*fvtx_.Z());
        }
	if(!fTMVAUpstreamCut){
	  fHisto->FillHisto("BDT_sig_mmiss_vs_p_AfterBDTCut",fp_,fmm2_);
	  fHisto->FillHisto("BDT_sig_Ytrim5_vs_Xtrim5_AfterBDTCut",ftrim5_x_,ftrim5_y_);
	  fHisto->FillHisto("BDT_sig_RStraw1_vs_Zvtx_AfterBDTCut",0.001*fvtx_.Z(),fstraw1_r_);
	  fHisto->FillHisto("BDT_sig_Yvtx_vs_Xvtx_AfterBDTCut",0.001*fvtx_.X(),0.001*fvtx_.Y());
	  fHisto->FillHisto("BDT_sig_YStraw1_vs_XStraw1_AfterBDTCut",fstraw1_x_,fstraw1_y_);
	  fHisto->FillHisto("BDT_sig_ThetaY_vs_ThetaX_AfterBDTCut",fthx_,fthy_);
	  fHisto->FillHisto("BDT_sig_Zvtx_vs_p_AfterBDTCut",fp_,0.001*fvtx_.Z());

	  if(fvtx_.Z()>165000.){
	    fHisto->FillHisto("BDT_sig_mmiss_vs_p_AfterBDTCut_Z165",fp_,fmm2_);
	    fHisto->FillHisto("BDT_sig_Ytrim5_vs_Xtrim5_AfterBDTCut_Z165",ftrim5_x_,ftrim5_y_);
	    fHisto->FillHisto("BDT_sig_RStraw1_vs_Zvtx_AfterBDTCut_Z165",0.001*fvtx_.Z(),fstraw1_r_);
	    fHisto->FillHisto("BDT_sig_Yvtx_vs_Xvtx_AfterBDTCut_Z165",0.001*fvtx_.X(),0.001*fvtx_.Y());
	    fHisto->FillHisto("BDT_sig_YStraw1_vs_XStraw1_AfterBDTCut_Z165",fstraw1_x_,fstraw1_y_);
	    fHisto->FillHisto("BDT_sig_ThetaY_vs_ThetaX_AfterBDTCut_Z165",fthx_,fthy_);
	  }
        }
      }
    }

    if(!fIsInTime) return 0;
    if(!(fp_>15 && fp_<35)) return 0;
    if(!fFlagSelectionForBDT && (fbox_cut_ || fbb_cut_ || fmult_tot_) ) return 0; // standard bb cuts
    if(!(fbox_cut_ || fbb_cut_ || fmult_tot_)){
      fHisto->FillHisto(Form("PNNMainRICH_StdBBcuts_r%d_mmiss_vs_p", fregion_),fp_,fmm2_);
      fHisto->FillHisto(Form("PNNMainRICH_StdBBcuts_r%d_Zvtx_vs_p", fregion_),fp_,0.001*fvtx_.Z());
    }
    if(fFlagSelectionForBDT && fTMVAUpstreamCut) return 0;
    
    fHisto->FillHisto(Form("PNNMainRICH_r%d_mmiss_vs_p", fregion_),fp_,fmm2_);
    fHisto->FillHisto(Form("PNNMainRICH_r%d_Zvtx_vs_p", fregion_),fp_,0.001*fvtx_.Z());
    fHisto->FillHisto(Form("PNNMainRICH_r%d_lambda_vs_p", fregion_),flambda_,fp_);
    fHisto->FillHisto(Form("PNNMainRICH_r%d_mmiss_vs_mmissNominalK", fregion_),fmm2_, fmm2_nomi_);
    fHisto->FillHisto(Form("PNNMainRICH_r%d_mmiss_vs_mmissRICH", fregion_),fmm2_, fmm2_rich_);

    return 1;
}
int PNNAnal::UpstreamBackgroundAnalysis() {
  
    //Apply pnn selection
    if(!fcalo_pid_) return 0; //CALO PID
    //if(!(fp_ > 5 && fp_ <= 40)) return 0; //15-35GeV/c cut
    if(fphoton_)    return 0; //Photon rejection cluster-based
    if (fmult_tot_BDTTrees) return 0;
    if (fbb_cut_BDTTrees) return 0;
    
    //if(fabs(fdt_kk_) > 0.6) return 0;
    //if(fabs(fkaon_->GetTGTK()-fpion_->GetTCHOD()) > 1.1) return 0;
    //if(fkaon_->GetGTKExtraHits()) return 0;
    bool match = fcda_ < 4;
    int category = GetCategory();

    fHisto->FillHisto("Upstream_mmiss_vs_p_1",fp_,fmm2_);
    fHisto->FillHisto("Upstream_mmiss_vs_mmissRICH_1",fmm2_,fmm2_rich_);
    fHisto->FillHisto("Upstream_mmiss_vs_mmissNominalK_1",fmm2_,fmm2_nomi_);
    fHisto->FillHisto("Upstream_mmiss_vs_zvtx_1",fvtx_.Z(), fmm2_);
    fHisto->FillHisto("Upstream_rstraw1_vs_zvtx_1",fvtx_.Z(), fstraw1_r_);
    fHisto->FillHisto("Upstream_y_vs_x_trim5_1",ftrim5_x_, ftrim5_y_);
    fHisto->FillHisto("Upstream_p_vs_zvtx_1",fvtx_.Z(), fp_);
    fHisto->FillHisto("Upstream_cda_vs_dt_1",fdt_kk_,fcda_);
    fHisto->FillHisto("Upstream_downstream_distance_1",fpion_->GetDownstreamDistance());
    fHisto->FillHisto("Upstream_dtkpi_vs_dtkk_1",fdt_kk_,fdt_kpi_);

    if(!match){

        if(category!=0){
            fHisto->FillHisto(Form("NoPi0Rej_Upstream_C%d_rstraw1_vs_zvtx", category),fvtx_.Z(),fstraw1_r_);
            fHisto->FillHisto(Form("NoPi0Rej_Upstream_C%d_trim5y_vs_trim5x", category),ftrim5_x_,ftrim5_y_);
        }
    }
    if(!frich_pid_) return 0; //RICH PID

    fHisto->FillHisto("Upstream_mmiss_vs_p_2",fp_,fmm2_);
    fHisto->FillHisto("Upstream_mmiss_vs_mmissRICH_2",fmm2_,fmm2_rich_);
    fHisto->FillHisto("Upstream_mmiss_vs_mmissNominalK_2",fmm2_,fmm2_nomi_);
    fHisto->FillHisto("Upstream_mmiss_vs_zvtx_2",fvtx_.Z(), fmm2_);
    fHisto->FillHisto("Upstream_rstraw1_vs_zvtx_2",fvtx_.Z(), fstraw1_r_);
    fHisto->FillHisto("Upstream_y_vs_x_trim5_2",ftrim5_x_, ftrim5_y_);
    fHisto->FillHisto("Upstream_p_vs_zvtx_2",fvtx_.Z(), fp_);
    fHisto->FillHisto("Upstream_cda_vs_dt_2",fdt_kk_,fcda_);
    fHisto->FillHisto("Upstream_dtkpi_vs_dtkk_2",fdt_kk_,fdt_kpi_);
    fHisto->FillHisto("Upstream_downstream_distance_2",fpion_->GetDownstreamDistance());

    //Test the weird component
    if (!match && (fevt_->GetIsHighToT() || fevt_->GetIsSignalInCHANTI() || fevt_->GetIsInteraction())){
        fHisto->FillHisto("Upstream_mmiss_vs_p_3",fp_,fmm2_);
        fHisto->FillHisto("Upstream_mmiss_vs_mmissRICH_3",fmm2_,fmm2_rich_);
        fHisto->FillHisto("Upstream_mmiss_vs_mmissNominalK_3",fmm2_,fmm2_nomi_);
        fHisto->FillHisto("Upstream_mmiss_vs_zvtx_3",fvtx_.Z(), fmm2_);
        fHisto->FillHisto("Upstream_rstraw1_vs_zvtx_3",fvtx_.Z(), fstraw1_r_);
        fHisto->FillHisto("Upstream_y_vs_x_trim5_3",ftrim5_x_, ftrim5_y_);
        fHisto->FillHisto("Upstream_p_vs_zvtx_3",fvtx_.Z(), fp_);
        fHisto->FillHisto("Upstream_cda_vs_dt_3",fdt_kk_,fcda_);
        fHisto->FillHisto("Upstream_dtkpi_vs_dtkk_3",fdt_kk_,fdt_kpi_);
        fHisto->FillHisto("Upstream_downstream_distance_3",fpion_->GetDownstreamDistance());
    }

    if (fcda_ < 2 && (fevt_->GetIsHighToT() || fevt_->GetIsSignalInCHANTI() || fevt_->GetIsInteraction())){
        fHisto->FillHisto("Upstream_mmiss_vs_p_4",fp_,fmm2_);
        fHisto->FillHisto("Upstream_mmiss_vs_mmissRICH_4",fmm2_,fmm2_rich_);
        fHisto->FillHisto("Upstream_mmiss_vs_mmissNominalK_4",fmm2_,fmm2_nomi_);
        fHisto->FillHisto("Upstream_mmiss_vs_zvtx_4",fvtx_.Z(), fmm2_);
        fHisto->FillHisto("Upstream_rstraw1_vs_zvtx_4",fvtx_.Z(), fstraw1_r_);
        fHisto->FillHisto("Upstream_y_vs_x_trim5_4",ftrim5_x_, ftrim5_y_);
        fHisto->FillHisto("Upstream_p_vs_zvtx_4",fvtx_.Z(), fp_);
        fHisto->FillHisto("Upstream_cda_vs_dt_4",fdt_kk_,fcda_);
        fHisto->FillHisto("Upstream_dtkpi_vs_dtkk_4",fdt_kk_,fdt_kpi_);
        fHisto->FillHisto("Upstream_downstream_distance_4",fpion_->GetDownstreamDistance());

    }
    if(fp_ <= 10 || fp_ > 40) return 0;

    if(!match){

        if(category!=0){
            fHisto->FillHisto(Form("Upstream_C%d_rstraw1_vs_zvtx", category),fvtx_.Z(),fstraw1_r_);
            fHisto->FillHisto(Form("Upstream_C%d_trim5y_vs_trim5x", category),ftrim5_x_,ftrim5_y_);
        }
    }

    if(fFlagSelectionForBDT && !match && fIsSignalRegions){
      fHisto->FillHisto("BDT_bkg_dtkpi_vs_dtkk",fdt_kk_,fdt_kpi_);
      if(fFlagFillBDTTrees && !fIsInTime){ // TRAINING sample (out of time)
	fp_tree = fp_ ;
	fmm2_tree = fmm2_ ;
	Xtrim5_tree = ftrim5_x_ ;
	Ytrim5_tree = ftrim5_y_ ;
	Xstraw1_tree = fstraw1_x_ ;
	Ystraw1_tree = fstraw1_y_ ;
	Rstraw1_tree = fstraw1_r_ ;
	Xvtx_tree = fvtx_.X() ;
	Yvtx_tree = fvtx_.Y() ;
	Zvtx_tree = fvtx_.Z() ;
	cda_tree = fcda_ ;
	ThetaX_tree = fthx_ ;
	ThetaY_tree = fthy_ ;
	Lambda_tree = flambda_ ;
	fTreeBkg->Fill();
	fHisto->FillHisto("BDT_bkg_mmiss_vs_p_Training",fp_,fmm2_);
	if(!(fbox_cut_ || fbb_cut_ || fmult_tot_)){
	  fHisto->FillHisto("BDT_bkg_mmiss_vs_p_TrainingAfterStandardCuts",fp_,fmm2_);
	}
      }
    
      if(fFlagReaderBDT && fIsInTime){ // PHYSICS sample (in time)
	fHisto->FillHisto("BDT_bkg_Classifier",fTMVAValue);
	if(!(fbox_cut_ || fbb_cut_ || fmult_tot_)){
	  fHisto->FillHisto("BDT_bkg_Classifier_AfterStandardCuts",fTMVAValue);
	  fHisto->FillHisto("BDT_bkg_dtkk_AfterStandardCuts",fabs(fdt_kk_));
	  fHisto->FillHisto("BDT_bkg_mmiss_vs_p_AfterStandardCuts",fp_,fmm2_);
	  fHisto->FillHisto("BDT_bkg_Zvtx_vs_p_AfterStandardCuts",fp_,0.001*fvtx_.Z());
	}
	if(!fTMVAUpstreamCut){// BDT cut for bkg contamination < 0.001
	  fHisto->FillHisto("BDT_bkg_dtkk_AfterBDTCut",fabs(fdt_kk_));
          fHisto->FillHisto("BDT_bkg_mmiss_vs_p_AfterBDTCut",fp_,fmm2_);
          fHisto->FillHisto("BDT_bkg_Ytrim5_vs_Xtrim5_AfterBDTCut",ftrim5_x_,ftrim5_y_);
          fHisto->FillHisto("BDT_bkg_RStraw1_vs_Zvtx_AfterBDTCut",0.001*fvtx_.Z(),fstraw1_r_);
          fHisto->FillHisto("BDT_bkg_Yvtx_vs_Xvtx_AfterBDTCut",0.001*fvtx_.X(),0.001*fvtx_.Y());
          fHisto->FillHisto("BDT_bkg_YStraw1_vs_XStraw1_AfterBDTCut",fstraw1_x_,fstraw1_y_);
          fHisto->FillHisto("BDT_bkg_ThetaY_vs_ThetaX_AfterBDTCut",fthx_,fthy_);
	  fHisto->FillHisto("BDT_bkg_Zvtx_vs_p_AfterBDTCut",fp_,0.001*fvtx_.Z());

          if(fvtx_.Z()>165000.){
            fHisto->FillHisto("BDT_bkg_mmiss_vs_p_Training_AfterBDTCut_Z165",fp_,fmm2_);
            fHisto->FillHisto("BDT_bkg_Ytrim5_vs_Xtrim5_AfterBDTCut_Z165",ftrim5_x_,ftrim5_y_);
            fHisto->FillHisto("BDT_bkg_RStraw1_vs_Zvtx_AfterBDTCut_Z165",0.001*fvtx_.Z(),fstraw1_r_);
            fHisto->FillHisto("BDT_bkg_Yvtx_vs_Xvtx_AfterBDTCut_Z165",0.001*fvtx_.X(),0.001*fvtx_.Y());
            fHisto->FillHisto("BDT_bkg_YStraw1_vs_XStraw1_AfterBDTCut_Z165",fstraw1_x_,fstraw1_y_);
            fHisto->FillHisto("BDT_bkg_ThetaY_vs_ThetaX_AfterBDTCut_Z165",fthx_,fthy_);
          }
        }
      }
    }

    if(match) return 0;
    if(!fIsInTime) return 0;
    if(!fFlagSelectionForBDT && (fbox_cut_ || fbb_cut_ || fmult_tot_) ) return 0; // standard bb cuts
    if(!(fbox_cut_ || fbb_cut_ || fmult_tot_) && fIsSignalRegions){
      fHisto->FillHisto("SampleC_dtkk_AfterStandardCuts",fabs(fdt_kk_));
      fHisto->FillHisto("SampleC_dtkpi_AfterStandardCuts",fabs(fdt_kpi_));
      fHisto->FillHisto("SampleC_dtkpi_vs_dtkk_AfterStandardCuts",fabs(fdt_kk_),fabs(fdt_kpi_));
      fHisto->FillHisto("SampleC_mmiss_vs_p_AfterStandardCuts",fp_,fmm2_);
    }
    if(fFlagSelectionForBDT && fTMVAUpstreamCut) return 0;

    // sample "C" selected at this point (full pinunu selection with inverted cda cut, and without kinematics regions)
    if(fIsSignalRegions){
      fHisto->FillHisto("SampleC_dtkk_AfterBDTCut",fabs(fdt_kk_));
      fHisto->FillHisto("SampleC_dtkpi_AfterBDTCut",fabs(fdt_kpi_));
      fHisto->FillHisto("SampleC_dtkpi_vs_dtkk_AfterBDTCut",fabs(fdt_kk_),fabs(fdt_kpi_));
      fHisto->FillHisto("SampleC_mmiss_vs_p_AfterBDTCut",fp_,fmm2_);
    }

    return 1;
}

void PNNAnal::DiscriminantNormalization() {

    double pass_dt = 0.001; // mm

    fPDFKaonDT_.clear();
    fIntPDFKaonDT_ = 0;
    for (int jbindt(0); jbindt<3000; jbindt++) { // +-3 ns
        double dt = pass_dt*jbindt+0.5*pass_dt;
        fIntPDFKaonDT_ += (PDFKaonDT(dt)+PDFKaonDT(-dt))*pass_dt/0.01; // compute the total integral of the PDF
        fPDFKaonDT_.push_back(fIntPDFKaonDT_); // fill the cumulative distribution function
    }
}

Double_t PNNAnal::Discriminant(const Double_t &dt_val) {
    double D = 0;

    if(fabs(dt_val) >= 3) return D;

    double pkdt    = 0;
    double pass_dt = 0.001;
    for (auto ikdt = fPDFKaonDT_.begin(); ikdt != fPDFKaonDT_.end();++ikdt) {
        double id = (double)(distance(fPDFKaonDT_.begin(),ikdt))*pass_dt; // compute the time corresponding to each element of the cumulative distribution function
        if(fabs(dt_val) < id) {
            pkdt = id/fIntPDFKaonDT_;

            break;
        }
    }

    D = (1 - pkdt);
    return D;
}
Double_t PNNAnal::PDFKaonDT(Double_t dt) {
    Double_t dtpar[3];

    dtpar[0] = 0.02;
    dtpar[1] = 0.;
    dtpar[2] = 0.2;

    Double_t dtg1 = dtpar[0]*exp(-0.5*((dt-dtpar[1])/dtpar[2])*((dt-dtpar[1])/dtpar[2]));
    return dtg1;
}

int PNNAnal::PionPID() {

    bool D = 0;

    if (fpion_->GetIsMUV3()) D = 1;
    if (fpion_->GetIsMulti()) D = 1;
    if (fpion_->GetIsMip()) D = 1;

    Double_t caloEnergy = fpion_->GetCaloEnergy();
    Double_t r1 = fpion_->GetMUV1Energy()/caloEnergy;
    Double_t r2 = fpion_->GetMUV2Energy()/caloEnergy;
    Double_t elkr = fpion_->GetLKrEnergy();
    Double_t seedratio = fpion_->GetLKrSeedEnergy()/elkr;
    Double_t cellratio = fpion_->GetLKrNCells()/elkr;
    bool isLKrEM = 0;
    auto vcut       = TMath::Max(0.7,0.98-0.4596*exp(-(fp_-11.44)/5.27));
    bool IsCaloPion = fpion_->GetCaloPionProb()>=vcut;

    if (r1<0.01 && r2<0.01) {
        if (seedratio>0.2&&cellratio<=3) isLKrEM = 1;
        if (seedratio<=0.2&&cellratio<1.8) isLKrEM = 1;
        if (seedratio>0.35) isLKrEM = 1;
        if (seedratio<0.05) isLKrEM = 1;
    }
    if (seedratio>0. && seedratio<0.8 && cellratio<1.4) isLKrEM = 1;

    if(!IsCaloPion) D = 1;
    if (isLKrEM) D = 1;
    if (caloEnergy>1.2*fp_) D = 1;
    if (elkr>0.8*fp_) D = 1;
    if (!fpion_->GetIsGoodMUV1() && fpion_->GetIsGoodMUV2()) D = 1;

    return D > 0 ? 0 : 1;
}


int PNNAnal::GetRegion(bool print) {
  
    double bound = Kmu2KinematicBound();

    bool isPNNRegion1 = (fmm2_ > 0 && fmm2_ <= 0.01) && (fmm2_nomi_ > -0.005 && fmm2_nomi_ <= 0.0135) && (fmm2_rich_ > 0. && fmm2_rich_ <= 0.01); // fp_ <= 20
    if(fp_ > 20 && fp_ <= 25)
        isPNNRegion1 = (fmm2_ > 0 && fmm2_ <= 0.01) && (fmm2_nomi_ > -0.005 && fmm2_nomi_ <= 0.0135) && (fmm2_rich_ > 0. && fmm2_rich_ <= 0.02);
    if(fp_ > 25)
        isPNNRegion1 = (fmm2_ > 0 && fmm2_ <= 0.01) && (fmm2_nomi_ > 0 && fmm2_nomi_ <= 0.0135)      && (fmm2_rich_ > -0.005 && fmm2_rich_ <= 0.02);

    bool isPNNRegion2 = fmm2_ <= 0.068 && fmm2_ > 0.026 && (fmm2_nomi_ > 0.024 && fmm2_nomi_ <= 0.068) && (fmm2_rich_ > 0.02 && fmm2_rich_ <= 0.07);
    bool isCR1        = fmm2_ > 0.01  && fmm2_<=0.015;
    bool isCR2        = fmm2_>0.021 && fmm2_ <= 0.026;
    bool isCRmu       = fmm2_<=0     && fmm2_>bound+0.0036;
    bool isCR3pi      = fmm2_>0.068 && fmm2_<=0.072;
    bool isCRUp       = (fmm2_>bound-0.0068 && fmm2_<=bound-0.05); // (fmm2_>bound-0.0068 && fmm2_<=bound-0.0060)
    bool isK2piRegion = (fmm2_>0.015 && fmm2_<=0.021);
    bool isKmu2Region = (fmm2_>-0.05 && fmm2_<=bound+0.0036) /*(fmm2_>bound-0.0060 && fmm2_<= bound+0.0036)*/;
    bool isK3PiRegion  = (fmm2_>0.072);
    bool isUpRegion   = fmm2_<=bound-0.0068;

    if (isPNNRegion1) return 1;
    if (isPNNRegion2) return 2;
    if (isCR1) return 10;
    if (isCR2) return 20;
    if (isCRmu) return 30;
    if (isCR3pi) return 40;
    if (isCRUp) return 50;
    if (isK2piRegion) return 100;
    if (isKmu2Region) return 200;
    if (isK3PiRegion) return 300;
    if (isUpRegion) return 400;

    return 0;
}
bool PNNAnal::IsSnake() {
    double b1 = -0.004;
    double a1 = 315-b1*105000.;
    double r115 = a1+b1*115000.;
    double b2 = -(900-r115)/10000.;
    double a2 = 900-b2*105000.;
    double b3 = -0.00983333333;
    double a3 = 780-b3*105000.;
    double cut1 = a1+b1*fvtx_.Z();
    double cut2 = a2+b2*fvtx_.Z();
    double cut3 = a3+b3*fvtx_.Z();
    bool isSignal = fstraw1_r_>cut1 && fstraw1_r_>cut2 && fstraw1_r_<cut3 && fvtx_.Z()<165000.;
    return !isSignal;
}

double PNNAnal::Kmu2KinematicBound(){

    double mpi = 0.13957018;
    double mmu = 0.1056583745;

    return (pow(mpi,2) - pow(mmu,2))*(1 - 75/fp_);
}

int PNNAnal::GetCategory(){

    int category = 0;
    //3D missing mass cut to be added
    bool r1  = fmm2_ <= 0.01 && fmm2_ > 0 ? true : false;
    bool r2  = fmm2_ <= 0.068 && fmm2_ > 0.026 ? true : false;

    if(r1){
        if(fp_ > 15 && fp_ <= 18) category = 11;
        if(fp_ > 18 && fp_ <= 25) category = 12;
        if(fp_ > 25 && fp_ <= 32) category = 13;
        if(fp_ > 32 && fp_ <= 35) category = 14;
        if(fp_ > 35 && fp_ <= 40) category = 15;
    }

    if(r2){
        if(fp_ > 15 && fp_ <= 18) category = 21;
        if(fp_ > 18 && fp_ <= 25) category = 22;
        if(fp_ > 25 && fp_ <= 32) category = 23;
        if(fp_ > 32 && fp_ <= 35) category = 24;
        if(fp_ > 35 && fp_ <= 40) category = 25;
    }

    return category;
}

void PNNAnal::EndOfJob(){
  if(fFlagFillBDTTrees){
    fTreeSig->Write();
    fTreeBkg->Write();
  }
  if(fFlagReaderBDT) fTMVAReader->Delete();
}
